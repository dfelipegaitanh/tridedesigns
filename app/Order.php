<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $fillable = ['name','email','address','city','state','zip','shipping' ];

    protected $casts = [
        'products' => 'array',
    ];

}
