<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Blog;
use Illuminate\Http\Request;
use Illuminate\support\Facades\DB;

class CommentController extends Controller
{
      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comment::orderBy('id','DESC')->paginate(10);
        return view('admin.comments.index',compact('comments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('admin.collection.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $comment = new Comment();
        $validatedData = $request->validate([
            'name'     => 'required',
            'email'     => 'required|email',
            'comment'     => 'required',
            'avatar'       => 'image|mimes:png,jpg,jpeg',
         ]);
         
        $comment->blog_id    = $request->id;  
        $comment->name    = $request->name;  
        $comment->email   = $request->email;  
        $comment->comment = $request->comment;  
        
        if($request->hasFile('avatar')){
            $comment->avatar = $request->avatar->store('uploads/comments/photos','public');
            //ImageOptimizer::optimize(base_path('public/').$product->thumbnail_img);
        }
        if($comment->save()) {
            $notification = array(
                'messege' => 'Thank you, your comment has been submitted and is pending review.',
                'alert-type' => 'success',
            );
            return Redirect()->back()->with($notification);
        }else{
            $notification = array(
                'messege' => 'Comment Failed',
                'alert-type' => 'error',
            );
            return Redirect()->back()->with($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $status = Comment::find($request->id);
        $status->status='a';    
        
        if($status->save()) {
            $notification = array(
                'messege' => 'Comment approved successfully',
                'alert-type' => 'success',
            );
            return Redirect()->back()->with($notification);
        }else{
            $notification = array(
                'messege' => 'Failed to approve comment',
                'alert-type' => 'error',
            );
            return Redirect()->back()->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     $collection = Comment::findOrFail($id);
     if($collection->delete()) {
        $notification = array(
            'messege' => 'Comment delete successfully',
            'alert-type' => 'success',
        );
        return Redirect()->back()->with($notification);
    }else{
        $notification = array(
            'messege' => 'Failed to delete comment',
            'alert-type' => 'error',
        );
        return Redirect()->back()->with($notification);
    }

    }
}
