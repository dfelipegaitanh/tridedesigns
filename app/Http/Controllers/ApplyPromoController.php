<?php

namespace App\Http\Controllers;

use App\Product;
use App\Promotion;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;

class ApplyPromoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $promotion = Promotion::orderBy('id', 'desc')->paginate(10);
        return view('admin.promotions.index', compact('promotion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        // return view('admin.collection.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return false|Response|string
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'promocode' => 'required',
        ]);
        if ($validator->fails()) {
            $response = ['type' => 'error', 'message' => 'Invalid Promo Code'];
            return json_encode($response);
        }

        $product = $this->getProduct();
        if (!$product) {
            return json_encode(['type' => 'error', 'message' => 'Invalid product identifier']);
        }

        $promotion = Promotion::where([
            ['name', $request->promocode],
            ['applyto', $product->collection_id]
        ])->first();
        if (!$promotion) {
            return json_encode(['type' => 'error', 'message' => 'Invalid Promo Code']);
        }

        if (!$this->validatePromoToProduct($promotion, $product)) {
            $response = ['type' => 'error', 'message' => 'This promo cant be applied to this product'];
            return json_encode($response);
        }

        $newPrice = $product->price - (($promotion->percentage / 100) * $product->price);
        $response = [
            'type'     => 'success',
            'message'  => 'Promo Applied successfully!',
            'newPrice' => $newPrice
        ];
        return json_encode($response);

    }

    private function getProduct()
    {
        $products = session()->get('products');
        $product  = collect();
        if ($products->first() !== null) {
            $product = $products->first();
            $product = $product['product'];
        }
        return $product;
    }

    /**
     * @param         $promotion
     * @param Product $product
     * @return bool
     */
    private function validatePromoToProduct($promotion, Product $product)
    {
        return $promotion->applyto == 0 || $promotion->applyto == $product->collection_id;
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int     $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $promotion = Promotion::findOrFail($id);
        if ($promotion->delete()) {
            $notification = [
                'messege'    => 'Promotion deleted successfully',
                'alert-type' => 'success',
            ];
            return Redirect()->back()->with($notification);
        } else {
            $notification = [
                'messege'    => 'failed to delete promotion',
                'alert-type' => 'error',
            ];
            return Redirect()->back()->with($notification);
        }

    }
}
