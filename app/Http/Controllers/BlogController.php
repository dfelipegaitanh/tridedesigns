<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BlogController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::orderBy('id','DESC')->paginate(10);
        return view('admin.blogs.index', compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blogs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $blog = new Blog();

        $validatedData = $request->validate([
            'title'              => 'required|unique:blogs',
            'tags'              => 'required',
            'description'       => 'required',
            'freature_image'       => 'required|image|mimes:png,jpg,jpeg',
         ]);

        $blog->title = $request->title;
        $blog->tags = $request->tags;
        $blog->description = $request->description;
        $photos = array();
        $blog->added_by = Auth::user()->id;
        $blog->slug = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->title)).'-'.rand(100,1000);



        if($request->hasFile('freature_image')){
            $blog->freature_image = $request->freature_image->store('uploads/blogs/photos','public');
            //ImageOptimizer::optimize(base_path('public/').$product->thumbnail_img);
        }

        
        if($blog->save()) {
            $notification = array(
                'messege' => 'Blog added successfully',
                'alert-type' => 'success',
            );
           return Redirect()->to('admin/blogs')->with($notification);
        }else{
            $notification = array(
                'messege' => 'Failed to added blog',
                'alert-type' => 'error',
            );
            return Redirect()->back()->with($notification);
        }

    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::findOrFail($id);
        return view('admin.blogs.edit',compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Blog::findOrFail($id);

        $validatedData = $request->validate([
            'title'              => 'required',
            'tags'              => 'required',
            'description'       => 'required',
            'freature_image'       => 'image|mimes:png,jpg,jpeg',
         ]);

        $product->title = $request->title;
        $product->tags = $request->tags;

        
        $product->description = $request->description;
        $photos = array();
        // $freature_image = $request->freature_image;
        $product->added_by = Auth::user()->id;




        if($request->hasFile('photos')){
            foreach ($request->photos as $key => $photo) {
                $path = $photo->store('uploads/blogs/photos','public');
                array_push($photos, $path);
                //ImageOptimizer::optimize(base_path('public/').$path);
            }
            $product->photos = json_encode($photos);
        }

        if($request->hasFile('freature_image')){
            $product->freature_image = $request->freature_image->store('uploads/blogs/photos','public');
            //ImageOptimizer::optimize(base_path('public/').$product->thumbnail_img);
        }
        
        if($product->save()) {
            $notification = array(
                'messege' => 'Blog updated successfully',
                'alert-type' => 'success',
            );
            return Redirect()->to('admin/blogs')->with($notification);
        }else{
            $notification = array(
                'messege' => 'Failed to update blog',
                'alert-type' => 'error',
            );
            return Redirect()->back()->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $blog = Blog::findOrFail($id);
        Comment::where('blog_id', $id)->delete();
        if($blog->delete()) {
            $notification = array(
                'messege' => 'Blog delete successfully',
                'alert-type' => 'success',
            );
            return Redirect()->to('admin/blogs')->with($notification);
        }else{
            $notification = array(
                'messege' => 'Failed to delete blog',
                'alert-type' => 'error',
            );
            return Redirect()->back()->with($notification);
        }
    }
}
