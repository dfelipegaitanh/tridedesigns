<?php

namespace App\Http\Controllers;

use App\Mail\OrderCreated;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Stripe\Balance;
use Stripe\Charge;
use Stripe\Stripe;
use Stripe\StripeClient;

class StripeController extends Controller
{

    /**
     * @var float
     */
    private $tax;

    public function __construct()
    {
        $this->tax = 1.07;
    }

    public function index(Order $order)
    {
        return view('stripe.index', compact('order'));
    }

    public function checkout(Order $order, Request $request)
    {

        $products = $order->getAttributeValue('products');

        $total = 0;
        foreach ($products as $product){
            $itemWithTax = $product['product']['price'] * $this->tax;
            $total += $itemWithTax * $product['qty'];
        }

        $secret_key = env('STRIPE_SECRET', '');
        Stripe::setApiKey($secret_key);
        $charge              = Charge::create([
            "amount"      => round($total),
            "currency"    => 'usd',
            "source"      => $request->stripeToken,
            "description" => "#$order->id Payment"
        ]);
        $order->status       = 1;
        $order->paypal_tx_id = $charge['balance_transaction'];
        $order->paypal_data  = json_encode($charge);
        $order->save();

        $data        = [];
        $data['txt'] = "You have receieved a new order, please check your admin panel for order details.";

        Mail::to(env('MAIL_TO_1', ''))
            // ->cc('order@hdl.tridedesigns.com')
            ->send(new OrderCreated($data));

        Mail::to(env('MAIL_TO_2', ''))
            // ->cc('order@hdl.tridedesigns.com')
            ->send(new OrderCreated($data));

        session()->forget('data');
        return view('frontend.thanks', compact('order'));
    }
}
