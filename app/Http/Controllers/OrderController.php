<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        abort(500);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $validatedData = $request->validate([
            'name'    => 'required',
            'email'   => 'required|email',
            'address' => 'required',
            'city'    => 'required',
            'state'   => 'required',
            'zip'     => 'required',
        ]);


        $order = new Order();

        $order->name        = $request->name;
        $order->email       = $request->email;
        $order->address     = $request->address;
        $order->city        = $request->city;
        $order->state       = $request->state;
        $order->zip         = $request->zip;
        $order->total       = $request->total;
        $order->product_id  = $request->product_id;
        $order->quantity    = $request->quantity;
        $order->unite_price = $request->unite_price;
        $order->session_id  = session_id();
        $order->shipping    = $request->shipping;

        $res = $order->save();

        if ($order->save()) {
            if ($request->method == 'paypal') {
                return redirect()->route('payple.payment');
            }
            if ($request->method == 'card') {
                return redirect()->route('stripe.payment', $order->id);
            }

            return redirect()->route('stripe.payment', $order->id);
        } else {
            return redirect()->back()->with('failed', 'Failed to save infomraton! please try again');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int     $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
