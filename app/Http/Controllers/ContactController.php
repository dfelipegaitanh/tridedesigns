<?php

namespace App\Http\Controllers;

use App\Mail\ContactMe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function store(Request $request) {

        $data            = [];
        $data['name']    = $request->name;
        $data['email']   = $request->email;
        $data['comment'] = $request->comment;

        Mail::to(env('MAIL_TO_1', ''))
            ->send(new ContactMe($data));

        Mail::to(env('MAIL_TO_2', ''))
            ->send(new ContactMe($data));

        return redirect()->back()->with('success', "Thanks for messaging us | We will contact you shortly");

    }
}
