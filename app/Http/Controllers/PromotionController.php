<?php

namespace App\Http\Controllers;

use App\Promotion;
use Illuminate\Http\Request;

class PromotionController extends Controller
{
      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promotion = Promotion::orderBy('id','desc')->paginate(10);
        return view('admin.promotions.index',compact('promotion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('admin.collection.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        $protmotion = new Promotion();

        $validatedData = $request->validate([
            'name'     => 'required',
            'discount'     => 'required|integer'
         ]);
         $protmotion->name = $request->name;
         $protmotion->percentage = $request->discount;
         $applyto = $request->cid;


        // Check if there is a promo code for the same collection
        $check = Promotion::where([['applyto', $applyto], ['name', $request->name]])->exists();
        if($check){
            $notification = array(
                'messege' => 'Promotion update Failed',
                'alert-type' => 'error',
            );
            return Redirect()->back()->with($notification);
        }

         if($applyto == 0){
             $protmotion->applyto = 0;
         }else{
             $protmotion->applyto = $request->cid;
         }

        if($protmotion->save()) {
            $notification = array(
                'messege' => 'Promotion added successfully',
                'alert-type' => 'success',
            );
            return Redirect()->back()->with($notification);
        }else{
            $notification = array(
                'messege' => 'Promotion update Failed',
                'alert-type' => 'error',
            );
            return Redirect()->back()->with($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     $promotion = Promotion::findOrFail($id);
     if($promotion->delete()) {
        $notification = array(
            'messege' => 'Promotion deleted successfully',
            'alert-type' => 'success',
        );
        return Redirect()->back()->with($notification);
    }else{
        $notification = array(
            'messege' => 'failed to delete promotion',
            'alert-type' => 'error',
        );
        return Redirect()->back()->with($notification);
    }

    }
}
