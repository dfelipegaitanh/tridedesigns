<?php

namespace App\Http\Controllers;

use App\Custom;
use App\Mail\CustomOrderCreated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;

class placeCustomOrder extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $order     = new Custom();
        $validator = \Validator::make($request->all(), [
            'name'       => 'required',
            'number'     => 'required',
            'email'      => 'required|email',
            'headsize'   => 'required',
            'hatpin'     => 'required',
            'additional' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }
        else {

            $data        = [];
            $data['txt'] = "You have received a new custom order, please login to your admin panel to review";


            Mail::to(env('MAIL_TO_1', ''))
                // ->cc('order@hdl.tridedesigns.com')
                ->send(new CustomOrderCreated($data));

            Mail::to(env('MAIL_TO_2', ''))
                // ->cc('order@hdl.tridedesigns.com')
                ->send(new CustomOrderCreated($data));

            $order->name         = $request->name;
            $order->number       = $request->number;
            $order->email        = $request->email;
            $order->color        = $request->color;
            $order->size         = $request->headsize;
            $order->hatpin       = $request->hatpin;
            $order->additioninfo = $request->additional;
            $order->save();
            return 1;
        }


    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $blog = Blog::findOrFail($id);
        return view('admin.blogs.edit', compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $product = Blog::findOrFail($id);

        $validatedData = $request->validate([
            'title'          => 'required',
            'tags'           => 'required',
            'description'    => 'required',
            'freature_image' => 'image|mimes:png,jpg,jpeg',
        ]);

        $product->title = $request->title;
        $product->tags  = $request->tags;


        $product->description = $request->description;
        $photos               = [];
        // $freature_image = $request->freature_image;
        $product->added_by = Auth::user()->id;


        if ($request->hasFile('photos')) {
            foreach ($request->photos as $key => $photo) {
                $path = $photo->store('uploads/blogs/photos', 'public');
                array_push($photos, $path);
                //ImageOptimizer::optimize(base_path('public/').$path);
            }
            $product->photos = json_encode($photos);
        }

        if ($request->hasFile('freature_image')) {
            $product->freature_image = $request->freature_image->store('uploads/blogs/photos', 'public');
            //ImageOptimizer::optimize(base_path('public/').$product->thumbnail_img);
        }

        if ($product->save()) {
            $notification = [
                'messege'    => 'Blog updated successfully',
                'alert-type' => 'success',
            ];
            return Redirect()->to('admin/blogs')->with($notification);
        }
        else {
            $notification = [
                'messege'    => 'Failed to update blog',
                'alert-type' => 'error',
            ];
            return Redirect()->back()->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $blog = Blog::findOrFail($id);
        Comment::where('blog_id', $id)->delete();
        if ($blog->delete()) {
            $notification = [
                'messege'    => 'Blog delete successfully',
                'alert-type' => 'success',
            ];
            return Redirect()->to('admin/blogs')->with($notification);
        }
        else {
            $notification = [
                'messege'    => 'Failed to delete blog',
                'alert-type' => 'error',
            ];
            return Redirect()->back()->with($notification);
        }
    }
}
