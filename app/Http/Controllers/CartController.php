<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class CartController extends Controller
{

    public function viewCart()
    {
        $products = session()->has('products') ? session()->get('products') : [];
        if (empty($products)) {
            return redirect(route('landing'));
        }
        return view('frontend.order', compact('products'));
    }

    public function stashItem($id)
    {
        $collection = !session()->has('products') ? collect() : session()->pull('products');
        $this->updateCollectionProducts($collection, decrypt($id));
        session(['products' => $collection]);

        return redirect()->route('cart.view');
    }

    /**
     * @param Collection $collection
     * @param            $id
     */
    private function updateCollectionProducts(Collection $collection, $id)
    {
        if ($collection->get($id) === null) {
            $collection->put($id, ['product' => Product::findOrFail($id), 'qty' => 0]);
        }
        $collection->put($id, $this->updateCollectionProduct($collection, $id));
    }

    /**
     * @param Collection $collection
     * @param            $id
     * @return mixed
     */
    private function updateCollectionProduct(Collection $collection, $id)
    {
        $product = $collection->get($id);
        $product['qty']++;
        return $product;
    }

    function updateQuantity(Request $request)
    {
        $collection     = !session()->has('products') ? collect() : session()->get('products');
        $product        = $collection->get($request->id);
        $product['qty'] = (int)$request->qty;
        $collection->put($request->id, $product);
        return response()->json(['success']);
    }

    function payNow(Request $request)
    {
        // return $request->all();
        $request->validate([
            'name'    => 'required',
            'email'   => 'required|email',
            'address' => 'required',
            'city'    => 'required',
            'state'   => 'required',
            'zip'     => 'required',
        ]);

        /*
             alter table orders drop column product_id;
             alter table orders drop column quantity;

             alter table orders
             add products longtext null;

            alter table orders alter column unite_price set default 0;
         */
        $order             = new Order();
        $order->products   = session()->get('products');
        $order->session_id = session_id();
        $order->fill($request->all(['name', 'email', 'address', 'city', 'state', 'zip', 'shipping']));

        if (!$order->save()) {
            return redirect()->back()->with('failed', 'Failed to save information! please try again');
        }

        session()->remove('products');
        switch ($request->get('method')) {
            case  'paypal':
                return redirect()->route('paypal.payment');
            /*
            case 'card':
                return redirect()->route('stripe.payment', $order->id);
            */
            default:
                return redirect()->route('stripe.payment', $order->id);
        }

    }

}
