<?php

namespace App\Http\Controllers;

use App\Custom;
use Illuminate\Http\Request;
use Illuminate\support\Facades\DB;

class CustomOrderController extends Controller
{
      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customs = Custom::orderBy('id','DESC')->paginate(10);
        return view('admin.custom-orders.index',compact('customs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $status = Custom::find($request->id);
        $status->status= 1;            
        if($status->save()) {
            $notification = array(
                'messege' => 'Order has been marked as complete',
                'alert-type' => 'success',
            );
            return Redirect()->back()->with($notification);
        }else{
            $notification = array(
                'messege' => 'Failed to mark the order as complete',
                'alert-type' => 'error',
            );
            return Redirect()->back()->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     $collection = Custom::findOrFail($id);
     if($collection->delete()) {
        $notification = array(
            'messege' => 'Comment delete successfully',
            'alert-type' => 'success',
        );
        return Redirect()->back()->with($notification);
    }else{
        $notification = array(
            'messege' => 'Failed to delete comment',
            'alert-type' => 'error',
        );
        return Redirect()->back()->with($notification);
    }

    }
}
