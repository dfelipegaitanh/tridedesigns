@extends('layouts.frontend.app')

@section('content')

<!-- Titlebar
================================================== -->
<section class="titlebar">
    <div class="container">
        <div class="sixteen columns">
            <h2>{{ 'HDL Designs Blog' }}</h2>				
        </div>
    </div>
    </section>

<!-- Content
================================================== -->

<!-- Container -->
<div class="container">

<div class="twelve columns">
	<div class="extra-padding">
		@foreach ($blogs as $blog)
		@php
			$comments = App\Comment::where('blog_id','=',$blog->id)->where('status','=','a')->count();

		@endphp
		<!-- Post -->
		<article class="post">
			<figure class="post-img">
				<a href="{{ url('post/'.$blog->slug)}}"><img src="{{ Storage::url($blog->freature_image) }}" style="height:100%;width: 100%;"/>			
				</a>
			</figure>

			<section class="date">
			<span class="day">{{date('d',strtotime($blog->created_at))}}</span>
				<span class="month">{{date('M',strtotime($blog->created_at))}}</span>
			</section>

			<section class="post-content">

				<header class="meta">
				<h2><a href="{{ url('post/'.$blog->slug)}}">{{ucwords($blog->title)}}</a></h2>
					<span><i class="fa fa-user"></i>By <a href="#">HDL Designs</a></span>
				<span><i class="fa fa-tag"></i><a href="#">{{$blog->tags}}</a></span>
					<span><i class="fa fa-comment"></i> <a href="#">{{$comments}} Comments</a></span>
				</header>

					
				<a href="{{ url('post/'.$blog->slug)}}" class="button color">Read More</a>

			</section>

		</article>
		<!-- Post / End -->
		
		@endforeach





		<div class="clearfix"></div>

		<!-- Pagination -->
		<div class="pagination-container">
			<nav class="pagination">
				<ul>
					{{ $blogs->links() }}   
				</ul>
			</nav>

		
		</div>

	</div>
</div>

<!-- Sidebar
================================================== -->
<div class="four columns">


	
	


	<!-- Tabs -->
	<div class="widget margin-top-40">

		<ul class="tabs-nav blog">
			<li class="active"><a href="#tab3">Recent</a></li>
		</ul>

		<!-- Tabs Content -->
		<div class="tabs-container">

	



			<div class="tab-content" id="tab3">
			
				<!-- Recent Posts -->
				<ul class="widget-tabs">
					
					@php
					$recentblogs = App\Blog::orderBy('id','desc')->limit(6)->get();
				@endphp
				@foreach ($recentblogs as $rBlog)
					<li>
						<div class="widget-thumb">
							<a href="{{ url('post/'.$rBlog->slug)}}"><img src="{{ Storage::url($rBlog->freature_image) }}" style="width: 100%;height:100%;" /></a>
						</div>
						
						<div class="widget-text">
							<h4><a href="{{ url('post/'.$rBlog->slug)}}">{{   ucwords($rBlog->title)   }}</a></h4>
							<span>{{date('M',strtotime($rBlog->created_at))}} {{date('d',strtotime($rBlog->created_at))}}, {{date('Y',strtotime($rBlog->created_at))}}</span>
						</div>
						<div class="clearfix"></div>
					</li>
			    @endforeach

				</ul>
			</div>
			
		</div>
		
	</div>
	
	

	
	<br>
	<br>

</div>


</div>
<!-- Container / End -->




</div>




@endsection