@extends('layouts.frontend.app')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
<style>
    .btn-primary, .btn-primary:focus {
        outline: none !important;
        color: #fff !important;
        padding: 10px 20px 10px 20px;
        background-color: #7a0400;
        border-color: #7a0400;
        border-radius: 0px;
        box-shadow: none  !important;
    }

    .btn-primary:hover, .btn-primary:active, .btn-primary:active:focus, .btn-primary:active:hover {
        color: #fff;
        background-color: #bb2621 !important;
        border-color: #bb2621 !important;
    }
    .sixteen.columns{
        display:none !important;
    }
</style>
@section('content')
    <div class="container">
        <div class="jumbotron text-center bg-white">
            <h1 class="display-3">Thank You!</h1>
            <p class="lead"><strong>Your Order has been confirmed.</strong></p>
            <!--<hr>-->
            <p>
                Questions regarding your order? Please <a href="{{url('contact')}}">Contact us</a>
            </p>
            <p class="lead">
                <a class="btn btn-primary" href="{{url('/')}}" role="button">BACK TO HOME</a>
            </p>
        </div>
    </div>
@endsection