@extends('layouts.frontend.app')
@section('content')

    <!-- Titlebar ================================================== -->
    <section class="titlebar margin-bottom-0">
        <div class="container">
            <div class="sixteen columns">
                <h2>FAQs</h2>
            </div>
        </div>
    </section>


    <!-- Content ================================================== -->
    <div class="container">
        <div class="sixteen columns">
            <h3 class="headline">Frequently Asked Questions</h3><span class="line margin-bottom-25"></span>
            <div class="clearfix"></div>
            
        </div>
    </div>

    <!-- Container -->
    <div class="container margin-bottom-10">

        <!-- Toggles -->
        <div class="twelve columns">
            <div class="extra-padding">

                <!-- Toggle 1 -->
                <div class="toggle-wrap faq">
                    <span class="trigger opened"><a href="#"><i
                                class="toggle-icon"></i>How long does it take to custom make a hat?</a></span>
                    <div class="toggle-container">
                        <p>We require 3-5 weeks for production depending on project.</p>
                    </div>
                </div>

                <!-- Toggle 2 -->
                <div class="toggle-wrap faq">
                    <span class="trigger"><a href="#"><i class="toggle-icon"></i>How do I find the right hat size for my custom order?</a></span>
                    <div class="toggle-container">
                        <p>Please refer to our blog title “How do you Figure out your hat size”</p>
                    </div>
                </div>

                <!-- Toggle 3 -->
                <div class="toggle-wrap faq">
                    <span class="trigger"><a href="#"><i class="toggle-icon"></i>Do you do special orders?</a></span>
                    <div class="toggle-container">
                        <p>We can do special orders or custom hats. Customizing includes specific crown shapes, brim
lengths, hat bands, finishes, colors, etc.</p>
                    </div>
                </div>

                <!-- Toggle 4 -->
                <div class="toggle-wrap faq">
                    <span class="trigger"><a href="#"><i class="toggle-icon"></i>Can I launch a custom order over the phone?</a></span>
                    <div class="toggle-container">
                        <p>No, please submit a custom order form agreeing to terms and conditions and request a call back
with additional questions</p>
                    </div>
                </div>

                <!-- Toggle 5 -->
                <div class="toggle-wrap faq">
                    <span class="trigger"><a href="#"><i class="toggle-icon"></i>Do you offer refunds for return?</a></span>
                    <div class="toggle-container">
                        <p>There are no refunds of any kind. You may exchange for equal or lesser value for unworn or
undamaged items.</p>
                    </div>
                </div>


            </div>
        </div>

        <!-- Sidebar -->
        <div class="four columns">
            <a href="contact">
                <div class="content-box color-effect-1">
                    <h3>Got More Question?</h3>

                    <div class="box-icon-wrap box-icon-effect-1 box-icon-effect-1a">
                        <div class="box-icon"><i class="fa  fa-mail-forward"></i></div>
                    </div>

                    <p>If you have more questions, send us a message and we will answer you as soon as possible.</p>
                </div>
            </a>
        </div>

    </div>
    <!-- Container / End -->

    <div class="margin-top-50"></div>
@endsection
