@extends('layouts.frontend.app')

@section('content')
    
<!-- Slider
================================================== -->
<div class="container fullwidth-element home-slider">

	<div class="tp-banner-container">
		<div class="tp-banner">
			<ul>

				<!-- Slide 1  -->
				<li data-transition="fade" data-slotamount="7" data-masterspeed="1000" style="height:85% !important;">
 					<img src="{{ asset($home_setting->banner) }}"  alt="darkblurbg"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
 					<div class="caption dark sfb fadeout" data-x="750" data-y="170" data-speed="400" data-start="800"  data-easing="Power4.easeOut">
						
						@if($home_setting->txt1status == 0)
						<h2>{{ $home_setting->text_one }}</h2>
						@endif
						@if($home_setting->txt2status == 0)
						<h3>{{ $home_setting->text_two }}</h3>
						@endif
						@if($home_setting->btnstatus == 0)
						<a href="{{ $home_setting->button_link }}" class="caption-btn">{{ $home_setting->button_text }}</a>
						@endif
					</div>
				</li>

				 {{-- DISABLED FOR NOW --}}
				{{-- Slide 2  --}}
				{{-- <li data-transition="zoomout" data-slotamount="7" data-masterspeed="1500" height="85%">
					<img src="images/slider.jpg"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
 					<div class="caption sfb fadeout" data-x="145" data-y="170" data-speed="400" data-start="800"  data-easing="Power4.easeOut">
						<h2>Dress Sharp</h2>
						<h3>Learn from the classics</h3>
						<a href="shop-with-sidebar.html" class="caption-btn">Shop The Collection</a>
					</div>
				</li> --}}


			 {{-- Slide 3  --}}
				{{-- <li data-transition="fadetotopfadefrombottom" data-slotamount="7" data-masterspeed="1000" height="85%">
 					<img src="images/slider3.jpg"  alt="darkblurbg"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
 					<div class="caption dark sfb fadeout" data-x="850" data-y="170" data-speed="400" data-start="800"  data-easing="Power4.easeOut">
						<h2>New In</h2>
						<h3>Pants and T-Shirts</h3>
						<a href="shop-with-sidebar.html" class="caption-btn">Shop The Collection</a>
					</div>
				</li> --}}
				
				
			</ul>
		</div>
	</div>
</div>


<!-- Featured 
================================================== 
<div class="container" >


	<div class="one-third column">
		<a href="#" class="img-caption" >
			<figure>
				<img src="images/featured_img_2.jpg" alt="" />
				<figcaption>
					<h3>FASCINATORS</h3>
					<span>Browse This Collection</span>
				</figcaption>
			</figure>
		</a>
	</div>

	<div class="one-third column">
		<a href="#" class="img-caption" >
			<figure>
				<img src="images/featured_img_3.jpg" alt="" />
				<figcaption>
					<h3>HDL CONTOURS</h3>
					<span>Browse This Collection</span>
				</figcaption>
			</figure>
		</a>
	</div>

</div> -->
<div class="clearfix"></div>


<!-- TESTIMONIALS -->
<div class="container">

	<!-- Headline -->
	<div class="sixteen columns">
		<h3 class="headline">TESTIMONIALS</h3>
		<span class="line margin-bottom-20"></span>
	</div>

	<!-- Navigation / Left -->
	<div id="showbiz_left_3" class="sb-navigation-left-2 alt"><i class="fa fa-angle-left"></i></div>

	<!-- ShowBiz Carousel -->
	<div id="happy-clients" class="showbiz-container sixteen carousel columns" >

	<!-- Portfolio Entries -->
	<div class="showbiz our-clients" data-left="#showbiz_left_3" data-right="#showbiz_right_3">
		<div class="overflowholder">

			<ul>

				<li>
					<div class="happy-clients-photo"><img src="images/happy-clients-01.jpg" alt="" /></div>
					<div class="happy-clients-cite">Love the detail and quality of the merchandise. It was exactly what I wanted. Will definitely purchase more items in the future.</div>
					<div class="happy-clients-author">Avery - Michigan</div>
				</li>
				
				<li>
					<div class="happy-clients-photo"><img src="images/happy-clients-02.jpg" alt="" /></div>
					<div class="happy-clients-cite">Demetrius D Lee and HDL Designs are the best when it comes to hat fashion. He has made me one of a kind pieces and he’s the only person I trust to crown my head with any hat or headpiece. Please patronize and support this awesome gift.</div>
					<div class="happy-clients-author">Bridgett - Texas</div>
				</li>
				
				<li>
					<div class="happy-clients-photo"><img src="images/happy-clients-03.jpg" alt="" /></div>
					<div class="happy-clients-cite">The level of creativity that the Hatman has is unparalleled! I've never seen ANYTHING like it! I recently told him that I wanted to upgrade my hat game, and boy did he give me an upgrade lol! He knows his clients, and he loves them. I don't know about anyone else but when I put on HDL design, it tends to make me walk a little bit taller and my back a little bit straighter. LONG LIVE THE HATMAN!!!</div>
					<div class="happy-clients-author">Tiffany - Ft. Lauderdale</div>
				</li>
				
				<li>
					<div class="happy-clients-photo"><img src="images/happy-clients-03.jpg" alt="" /></div>
					<div class="happy-clients-cite">HDL Designs is one of a kind with creative colors and amazing design work and l am in love with all the headpieces that is great for me to style on my head.</div>
					<div class="happy-clients-author">Akisha - Ft. Lauderdale</div>
				</li>

			</ul>
			<div class="clearfix"></div>

		</div>
		<div class="clearfix"></div>

	</div>
	</div>

	<!-- Navigation / Right -->
	<div id="showbiz_right_3" class="sb-navigation-right-2 alt"><i class="fa fa-angle-right"></i></div>

</div>
</div>
<!-- TESTIMONIAL / End -->


<!-- New Arrivals
================================================== -->
<div class="container">
	<!-- Headline -->
	<div class="sixteen columns" style="margin-bottom:10px;">
		<h3 class="headline">Collections</h3>
		<span class="line margin-bottom-0"></span>
	</div>

@php
$collection = App\Collection::orderBy('id', 'DESC')->limit(3)->get();	
@endphp
		@foreach ($collection as $colec)
<div class="one-third column">
	<a href="{{ url('shop/'.$colec->slug) }}" class="img-caption">
		<figure>
		    @if($colec->image)
			<img src="{{ Storage::url($colec->image) }}" style="width:100%;">
			@else
			<img src="{{ Storage::url('uploads/collection/featured_img_1.jpg') }}">
			@endif
			<figcaption>
			<h3>{{ucfirst($colec->name)}}</h3>
				<span>Explore Collection</span>
			</figcaption>
		</figure>
	</a>
</div>
@endforeach

</div>

<div class="container">

	<!-- Headline -->
	<div class="sixteen columns">
		<h3 class="headline">NEW ADDITIONS</h3>
		<span class="line margin-bottom-0"></span>
	</div>

	<!-- Carousel -->
	<div id="new-arrivals" class="showbiz-container sixteen columns" >

		<!-- Navigation -->
		<div class="showbiz-navigation">
			<div id="showbiz_left_1" class="sb-navigation-left"><i class="fa fa-angle-left"></i></div>
			<div id="showbiz_right_1" class="sb-navigation-right"><i class="fa fa-angle-right"></i></div>
		</div>
		<div class="clearfix"></div>

		<!-- Products -->
		<div class="showbiz" data-left="#showbiz_left_1" data-right="#showbiz_right_1" data-play="#showbiz_play_1" >
			<div class="overflowholder">

				<ul>
	@php
		$recent_product = App\Product::orderBy('id', 'DESC')->limit(10)->get();	
	@endphp
				@foreach ($recent_product as $product)
				<li>
					<figure class="product">
						<div class="mediaholder">
							<a href="{{ url('product/'.$product->slug)}}">
								<img alt="" src="{{ Storage::url($product->freature_image) }}" style="width: 100%;"/>
								<div class="cover">

									@if(is_array(json_decode($product->photos)))
									@foreach (json_decode($product->photos) as $key => $photo)									  										
										  <img  src="{{ Storage::url($photo) }}" style="width: 100%;">																  
									@endforeach
									@else
									<img alt="" src="{{ Storage::url($product->freature_image) }}" style="width: 100%;"/>
								  @endif							
								</div>
							</a>
							<a href="{{ url('product/'.$product->slug)}}" class="product-button">VIEW HAT</a>
						</div>

						<a href="{{ url('product/'.$product->slug)}}">
							<section>
								<span class="product-category">{{ $product->collection->name }}</span>
								<h5>{{ ucwords($product->name) }}</h5>
								<span class="product-price">
								    @if($product->status == 'a')
								    ${{ $product->price }}
								    @else
								    <span style="color:#7a0400;">SOLD</span>
								    @endif
								</span>
							</section>
						</a>
					</figure>
				</li>
				@endforeach
				


				</ul>
				<div class="clearfix"></div>

			</div>
			<div class="clearfix"></div>
		</div>
	</div>

</div>

<!-- CUSTOM ORDER BANNER -->
<div class="container">
	<div class="sixteen columns">

		<div class="info-banner">
			<div class="info-content">
				<h3>HAVE SOMETHING DIFFERENT IN MIND?</h3>
				<p>Get a custom order, your hat, your style!</p>
			</div>
			<a href="#" class="button color" onclick="document.getElementById('id01').style.display='block'">CUSTOM ORDER</a>
			<div class="clearfix"></div>
		</div>

	</div>
</div>
<!-- CUSTOM ORDER BANNER / End -->

<div class="container">

	<!-- Headline -->
	<div class="sixteen columns" style="margin-top: 10px;">
		<h3 class="headline">Recent Blogs</h3>
		<span class="line margin-bottom-30"></span>
	</div>

	<!-- Post #1 -->
	@php
		$recentblogs = App\Blog::orderBy('id','DESC')->limit(4)->get();
	@endphp
	@foreach ($recentblogs as $rBlog)
	
	<div class="four columns" style="border: 2px solid #f6f6f6;">
		<article class="from-the-blog">
			<figure class="from-the-blog-image">
				<a href="blog-single-post.html"><img src="{{ Storage::url($rBlog->freature_image) }}" style="width: 100%;" alt=""></a>
				<div class="hover-icon"></div>
			</figure>

			<section class="from-the-blog-content">
			<a href="blog-single-post.html"><h6>{{   ucwords($rBlog->title)   }}</h6></a>
				<i>By HDL Designs on {{date('M',strtotime($rBlog->created_at))}} {{date('d',strtotime($rBlog->created_at))}}</i>
		
				<a href="{{ url('post/'.$rBlog->slug)}}" class="button gray">Read More</a>
			</section>

		</article>
	</div>
	@endforeach






<!-- INSTAGRAM FEED -->
<div class="container">
	<div class="sixteen columns" style="margin-top: 10px;">
		<h3 class="headline">SOCIAL FEED</h3>
		<span class="line margin-bottom-50"></span>
	</div>

    <div class="sixteen columns">
        <script src="https://apps.elfsight.com/p/platform.js" defer></script>
<div class="elfsight-app-b011817e-468a-47a8-a2f0-ade5399aacdb"></div>

	</div>
</div>
<!-- INSTAGRAM FEED END -->

<div class="margin-top-50"></div>


@endsection