@extends('layouts.frontend.app')
@section('content')
    <style>b {
            font-weight: bold;
        }</style>
    <!-- Titlebar ================================================== -->
    <section class="titlebar margin-bottom-0">
        <div class="container">
            <div class="sixteen columns">
                <h2>DELIVERY & RETURNS</h2>
            </div>
        </div>
    </section>


    <!-- Content ================================================== -->
    <div class="container">
        <div class="row">
            <div class="col-sm">

                <br><br>

                <h3>Risk of Loss</h3> <br>
                If the Customer provides Hdldesigns.com with customer’s carrier account number or selects a
                carrier other than a carrier that regularly ships for Hdldesigns.com, title to products and risk of
                loss or damage during shipment passes from Hdldesigns.com to Customer upon shipment from
                Hdldesigns.com facility. For all other shipments, title to products and risk of loss or damage
                during shipment passes from Hdldesigns.com to Customer upon receipt by Customer.
                Notwithstanding the foregoing, title to software will remain with the applicable licensor(s) and
                Customer's rights therein are contained in the license agreement between such licensor(s) and
                Customer. Hdldesigns.com retains a security interest in the products until payment in full is
                received. Customer will be responsible for all shipping and related charges.
                <br><br>
                <h3>Return & Exchange Policy</h3><br>
                <p>Please follow these few steps to ensure that your <b>EXCHANGE</b> will be handled accurately and
                    quickly: <b>(Un-worn merchandise only please)</b></p>
                <ul>
                    <li>1. If you are unhappy with your purchase, we will gladly exchange an item for equal or
                        lesser value 7 days upon receiving product to return it in its original condition.
                    </li>
                    <li>2. There will be <b>no refund</b> of any kind and only allowed to make only 1 exchange.</li>
                    <li>3. You must contact us to confirm your return and the location of where to send the item.</li>
                    <li>4. You are responsible for shipping the item(s) back to us.</li>
                    <li>5. If the hat has any kind of tag on it, removal of the tag will void any exchange. Do not
                        remove any tags. In the same regard, any hat with size reducing tape adhered (stuck) to
                        the interior of the sweatband is considered worn and NOT returnable.
                    </li>
                    <li>6. There is no exchange on custom orders, clearance items and Inspirations Kaps.</li>
                    <li>7. Please pack the hat carefully and insure the box. You may send your order back to us
                        through the shipping carrier of your choice. We are not responsible for hats damaged in
                        shipping. Make sure that the box that you return the hat in is in good, strong condition.
                        If the box that we sent the hat to you in is now weak or damaged, please use a new box
                        to return your hat in.
                    </li>
                </ul>
                <br><br>
                <h3>Returning Damaged / Defective Items</h3><br>
                In cases of damage or defect, the return process can often be expedited by providing a digital
                image of the damage or defect (along with a clear description of the problem) in an email to
                hdldesigns2013@gmail.com. If the damage / defect cannot be verified over the phone or via
                email contact, the item may be required to return to HDL Designs for inspection before a
                determination can be made as to the state of the product within 7 calendar days.
                <br><br>
                <h3>Shipping</h3><br>
                We can ship to virtually any address in the world. Note that there are restrictions on some
                products, and some products cannot be shipped to international destinations which is
                determined by the shipping courier.
                Orders are shipped via USPS (United States Postal Service) or UPS. Most items/orders will ship
                within 3-5 business days. Please allow 7-21 days for shipping internationally with an additional
                fee.
                <br><br>


            </div>
        </div>
    </div>

    <!-- Container -->
    <div class="container margin-top-50">
        <div class="four columns">

            <!-- Information -->
            <div class="widget margin-top-10">
                <div class="accordion">
                    <!-- Section 3 -->


                </div>
            </div>

            <!-- Social -->
            <div class="widget">

                <div class="clearfix"></div>
                <br>
            </div>

        </div>

        <div class="margin-top-50"></div>
    </div>
@endsection
