
@extends('layouts.frontend.app')

@section('content')
<style>b{font-weight: bold !important;}</style>
<!-- Titlebar
================================================== -->
<section class="titlebar">
<div class="container">
	<div class="sixteen columns">
		<h2>Single Post</h2>
		
		<nav id="breadcrumbs">
			<ul>
				<li><a href="/">Home</a></li>
				<li><a href="/blog">Blog</a></li>
			<li>{{$post->title}}</li>
			</ul>
		</nav>
	</div>
</div>
</section>


<!-- Content
================================================== -->


<!-- Container -->
<div class="container">

<div class="twelve columns">
	<div class="extra-padding">

		<!-- Post -->
		<article class="post single">

			<figure class="post-img">
				<a href="{{ Storage::url($post->freature_image) }}" class="mfp-image" title="{{ucwords($post->title)}}"><img src="{{ Storage::url($post->freature_image) }}" style="width: 100%;"/>
				
				</a>
			</figure>

			<section class="date">		
				<span class="day">{{date('d',strtotime($post->created_at))}}</span>
				<span class="month">{{date('M',strtotime($post->created_at))}}</span>
			</section>

			<section class="post-content">
				@php
				$comments = App\Comment::where('blog_id','=',$post->id)->where('status','=','a')->count();
				@endphp
				<header class="meta">
					<h2><a href="#">{{ucwords($post->title)}}</a></h2>
					<span><i class="fa fa-user"></i>By <a href="#">HDL Designs</a></span>
					<span><i class="fa fa-tag"></i><a href="#">{{$post->tags}}</a></span>
					<span><i class="fa fa-comment"></i><a href="#">{{$comments}} Comments</a></span>
				</header>
				<div class="row">
					<div class="col-sm-7">
						{!!$post->description!!}</div>	
				</div>	
				<!-- Share Buttons -->	
				<div class="share-buttons">
					<ul>
						<li><a href="#">Share</a></li>
						<li class="share-facebook"><a href="#">Facebook</a></li>
						<li class="share-twitter"><a href="#">Twitter</a></li>
						<li class="share-gplus"><a href="#">Google Plus</a></li>
						<li class="share-pinit"><a href="#">Pin It</a></li>
					</ul>
				</div>
				<div class="clearfix"></div>

			</section>

		</article>
		<!-- Post / End -->


<!-- Comments
================================================== -->
	<h3 class="headline">Comments <span class="comments-amount">({{$comments}})</span></h3><span class="line"></span><div class="clearfix"></div>
	
		<!-- Reviews -->
		<section class="comments">

			<ul>
				@if($comments >0)
				@php
					$comments_list = App\Comment::where('blog_id','=',$post->id)->where('status','=','a')->get();
				@endphp
				@foreach ($comments_list as $comment)
				<li>
					<div class="avatar">
					    @if($comment->avatar)
					    <img src="{{ Storage::url($comment->avatar) }}" alt="">
					    @else
					    <img src="http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&amp;s=70" alt="">
					    @endif
					 </div>
					<div class="comment-content"><div class="arrow-comment"></div>
				<div class="comment-by"><strong>{{$comment->name}}</strong><span class="date">{{date('M d, Y',strtotime($comment->created_at))}}</span>
						
						</div>
					<p>{{$comment->comment}}</p>
					</div>
					<!-- Second Level -->			
				</li>
				@endforeach
				@else 
				<h3>Be First to comments</h3>
				@endif

			 </ul>

		</section>
		<div class="clearfix"></div>
		<br>


<!-- Add Comment
================================================== -->

	<h3 class="headline">Add Comment</h3><span class="line margin-bottom-35"></span><div class="clearfix"></div>
	
	<!-- Add Comment Form -->
	<form id="add-comment" class="add-comment" method="post" action="{{ url('post/store') }}" enctype="multipart/form-data">
		@csrf
		<fieldset>

			<div>
				<label>Name:</label>		
				<input type="text" name="name"  required=""/>
			</div>
				
			<div>
				<label>Email: <span>*</span></label>
				<input type="email" name="email"  required=""/>
			</div>
                
            
			<div>
				<label>Photo: <span>*</span></label>
				<input type="file" name="avatar"/>
			</div>    
                
			<div>
				<label>Comment: <span>*</span></label>
				<textarea cols="40" name="comment" rows="3" required=""></textarea>
			</div>

		</fieldset>
		<input type="hidden" name="id" value="{{ $post->id }}" />
		<button type="submit" style="background: #7a0400;color: white;font-weight: 600;height: 40px;cursor: pointer;outline: auto;text-transform: uppercase;">Add Comment</button>

		<div class="clearfix"></div>

	</form>
	


	</div>
</div>

<!-- Sidebar
================================================== -->
<div class="four columns">




	<!-- Tabs -->
	<div class="widget margin-top-40">

		<ul class="tabs-nav blog">
		
			<li class="active"><a href="#tab3">Recent</a></li>
		</ul>

		<!-- Tabs Content -->
		<div class="tabs-container">



			<div class="tab-content" id="tab3">
			
				<!-- Recent Posts -->
				<ul class="widget-tabs">
					@php
					$recentblogs = App\Blog::orderBy('id','desc')->limit(6)->get();
				@endphp
				@foreach ($recentblogs as $rBlog)
					<li>
						<div class="widget-thumb">
							<a href="{{ url('post/'.$rBlog->slug)}}"><img src="{{ Storage::url($rBlog->freature_image) }}" style="height:100%;width: 100%;"  /></a>
						</div>
						
						<div class="widget-text">
							<h4><a href="{{ url('post/'.$rBlog->slug)}}">{{   ucwords($rBlog->title)   }}</a></h4>
							<span>{{date('M',strtotime($rBlog->created_at))}} {{date('d',strtotime($rBlog->created_at))}}, {{date('Y',strtotime($rBlog->created_at))}}</span>
						</div>
						<div class="clearfix"></div>
					</li>
			    @endforeach
				</ul>
			</div>
			
		</div>
		
	</div>
	
	


	<br>
	<br>

</div>


</div>
<!-- Container / End -->

<div class="margin-top-40"></div>

@endsection