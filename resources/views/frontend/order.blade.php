@extends('layouts.frontend.app')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
@section('content')
    <style>
        .btn-primary {
            color: #fff;
            background-color: #7a0400;
            border-color: #7a0400;
        }
    </style>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <div class="container">
        <form action="{{ route('cart.payNow') }}" method="POST" id="orderForm">
            @csrf
            <div class="table-responsive mt-4">
                <table class="table table-bordered text-center">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Sub Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td class="d-flex justify-content-start">
                                <img src="{{ Storage::url($product['product']->freature_image) }}" alt="Product image"
                                     style="width: 50px;height: 50px;float: left;">
                                <span
                                    style="float: left;margin-left: 20px;margin-top: 5px;"> {{ $product['product']->name }} </span>
                            </td>
                            <td>
                                $<span id="order_price">{{ $product['product']->price }}</span>
                            </td>
                            <td>
                                <input type="number" min="1" value="{{$product['qty']}}"
                                       name="quantity[{{ $product['product']->id }}]"
                                       id="quantity-{{$product['product']->id}}"
                                       onchange="updateSubTotalItem('#newprice-{{$product["product"]->id}}' , {{$product['product']->price}} , this )"
                                       onblur="updateSessionQuantity( {{$product["product"]->id}}, this) "
                                       style="width: 60px;border: 2px solid #d3cccc;text-align: center;font-weight:
                                500;border-radius: 4px;">
                            </td>
                            <td> $ <input type="number" value="{{ $product['product']->price * $product['qty']}}"
                                          class="subTotals"
                                          id="newprice-{{$product['product']->id}}"
                                          style="width: 70px;border: 2px solid #d3cccc;text-align: center;font-weight: 500;border-radius: 4px;background: #cccccc"
                                          readonly></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="row">
                <div class="col-sm-7 col-lg-7">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Promo Code</label>
                                <input type="text" class="form-control" name="promocode" id="promocode"
                                       value="{{old('promocode')}}">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <a href="#" class="btn btn-primary" id="applypromo" style="color:white;margin-top: 11%;">Apply
                                Code</a>
                        </div>
                    </div>

                </div>
                <div class="col-sm-5 col-lg-5" id="promoresponse" style="margin-top:1.5%;">
                    <table class="table table-bordered text-center">
                        <thead>
                        <tr>
                            <th>Tax</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>7%</td>
                            <td> $ <input type="number" value="0" id="total-cart"
                                          name="total"
                                          style="width: 70px;border: 2px solid #d3cccc;text-align: center;font-weight: 500;border-radius: 4px;background: #cccccc"
                                          readonly></td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="shiping-info">
                        <div class="shiping-header pb-4">
                            <h3 class="text-center" style="position: relative">SHIPPING DETAILS</h3>
                            <hr class="pb-3"
                                style="width: 98px;border-top: 2px solid #ccc;position: absolute;top: 24px;left: 45%;">

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                        </div>

                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" class="form-control" name="name" value="{{old('name')}}">
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" name="email" value="{{old('email')}}">
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label>Address</label>
                            <textarea name="address" class="form-control">{{old('address')}}</textarea>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>City</label>
                                    <input type="text" name="city" class="form-control" value="{{old('city')}}">
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>State</label>
                                    <input type="text" name="state" class="form-control" value="{{old('state')}}">
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>Zip</label>
                                    <input type="text" name="zip" class="form-control" value="{{old('zip')}}">
                                </div>
                            </div>

                            <div class="col-md-3 col-lg-3">
                                <div class="form-group">
                                    <label>Shipping</label>
                                    <select class="form-control" name="shipping" ==>
                                    <option value="shipfree">Ship for Free</option>
                                    <option value="freepickup">Free Pickup</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" class="pay-with" name="method" value="">
                        @if( false )
                            <div class="form-group">
                                <label>Total</label>
                                <input type="text" name="total" id="totalCart" class="form-control">
                            </div>
                        @endif
                        <div class="form-group text-center">
                            <button class="btn btn-primary rounded-0 pay-with-card" type="submit">PAY NOW</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('footer-script')
    <script>
        function updateSubTotalItem(id, price, element) {
            $(id).val(parseFloat(element.value) * price);
            updateTotal();
        }

        function updateSessionQuantity(id, element) {

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    id: id,
                    qty: element.value
                },
                url: "{{route('cart.updateSessionQuantity')}}",
                type: "POST",
                success: function (response) {
                }
            });

        }

        function updateTotal() {
            $("#total-cart").val(0);
            $(".subTotals").each(function () {
                let itemValueTax = parseFloat($(this).val() * 1.07);
                let total = parseFloat($("#total-cart").val());
                $("#total-cart").val(parseFloat(itemValueTax + total).toFixed(2));
            });

        }

        let promoPrice = 0;
        $("#applypromo").click(function () {

            $.ajax({
                url: "{{route('applypromo')}}",
                type: "POST",
                data: {
                    promocode: $("#promocode").val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {

                    @if(false)
                        promoPrice = 0;
                    data = JSON.parse(response);
                    if (data.type == 'error') {
                        $("#promoresponse").html("<div class='alert alert-danger' role='alert'>" + data.message + "</div>");
                    } else {
                        $("#promoresponse").html("<div class='alert alert-success' role='alert'>" + data.message + "</div>");
                        let quantity = $('#quantity-{{$product->id}}').val();
                        let newPrice = parseFloat(((quantity - 1) * price) + data.newprice).toFixed(2);
                        $('#total-{{$product->id}}').val(calculateTotal(newPrice));
                        $('#newprice-{{$product->id}}').val(newPrice);
                        promoPrice = data.newprice;
                    }
                    @endif

                }

            });
        });


        $(document).on('click', '.pay-with-card', function () {
            $(".pay-with").val('card');
            $('form#orderForm').submit();
        })


        $(document).on('click', '.pay-with-paypal', function () {
            $(".pay-with").val('paypal');
            $('form#orderForm').submit();
        })


        setTimeout(updateTotal, 1000);
    </script>
@stop

