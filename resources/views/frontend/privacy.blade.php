@extends('layouts.frontend.app')
@section('content')
    <style>b {
            font-weight: bold;
        }</style>
    <!-- Titlebar ================================================== -->
    <section class="titlebar margin-bottom-0">
        <div class="container">
            <div class="sixteen columns">
                <h2>HDL DESIGNS PRIVACY POLICY</h2>
            </div>
        </div>
    </section>


    <!-- Content ================================================== -->
    <div class="container">
        <div class="row">
            <div class="col-sm">
                <br><br>
                HDL Designs is committed to maintaining robust privacy protections for its users. Our Privacy Policy
                (“Privacy Policy”) is designed to help you understand how we collect, use and safeguard the information
                you provide to us and to assist you in making informed decisions when using our Service

                <br><br>
                <h3>SECTION 1 – WHAT DO WE DO WITH YOUR INFORMATION?</h3> <br>
                When you purchase something from our store, as part of the buying and selling process, we collect the
                personal information you give us such as your name, address and email address.<br><br>
                When you browse our store, we also automatically receive your computer’s internet protocol (IP) address
                in order to provide us with information that helps us learn about your browser and operating system.<br><br>
                Email marketing (if applicable): With your permission, we may send you emails about our store, new
                products and other updates.
                <br><br>
                <h3>SECTION 2 – CONSENT</h3> <br>
                How do you get my consent?
                When you provide us with personal information to complete a transaction, verify your credit card, place
                an order, arrange for a delivery or exchange a purchase, we imply that you consent to our collecting it
                and using it for that specific reason only.<br><br>
                If we ask for your personal information for a secondary reason, like marketing, we will either ask you
                directly for your expressed consent, or provide you with an opportunity to say no.<br><br>
                How do I withdraw my consent?
                If after you opt-in, you change your mind, you may withdraw your consent for us to contact you, for the
                continued collection, use or disclosure of your information, at any time, by contacting us at
                hdldesigns2013@gmail.com.
                <br><br>
                <h3>SECTION 3 – DISCLOSURE</h3> <br>
                We may disclose your personal information if we are required by law to do so or if you violate our Terms
                of Service.
                <br><br>
                <h3>SECTION 4 – THIRD-PARTY SERVICES</h3> <br>
                In general, the third-party providers used by us will only collect, use and disclose your information to
                the extent necessary to allow them to perform the services they provide to us.<br><br>
                However, certain third-party service providers, such as payment gateways and other payment transaction
                processors, have their own privacy policies in respect to the information we are required to provide to
                them for your purchase-related transactions.<br><br>
                For these providers, we recommend that you read their privacy policies so you can understand the manner
                in which your personal information will be handled by these providers.<br><br>
                In particular, remember that certain providers may be located in or have facilities that are located a
                different jurisdiction than either you or us. So if you elect to proceed with a transaction that
                involves the services of a third-party service provider, then your information may become subject to the
                laws of the jurisdiction(s) in which that service provider or its facilities are located.<br><br>
                As an example, if you are located in Canada and your transaction is processed by a payment gateway
                located in the United States, then your personal information used in completing that transaction may be
                subject to disclosure under United States legislation, including the Patriot Act.<br><br>
                Once you leave our store’s website or are redirected to a third-party website or application, you are no
                longer governed by this Privacy Policy or our website’s Terms of Service.
                Links
                When you click on links on our store, they may direct you away from our site. We are not responsible for
                the privacy practices of other sites and encourage you to read their privacy statements.
                <br><br>
                <h3>SECTION 5 – SECURITY</h3><br>
                To protect your personal information, we take reasonable precautions and follow industry best practices
                to make sure it is not inappropriately lost, misused, accessed, disclosed, altered or destroyed.
                If you provide us with your credit card information, the information is encrypted using secure socket
                layer technology (SSL) and stored with a AES-256 encryption. Although no method of transmission over the
                Internet or electronic storage is 100% secure, we follow all PCI-DSS requirements and implement
                additional generally accepted industry standards.
                <br><br>
                <h3>SECTION 6 – COOKIES</h3> <br>
                Cookies are small pieces of data, stored in text files, that are stored on your computer or other device
                when websites are loaded in a browser. They are widely used in order to make websites work, or work more
                efficiently, as well as to provide information to the owners of the site.
                <br><br>
                <h3>SECTION 7 – AGE OF CONSENT
                </h3><br>
                By using this site, you represent that you are at least the age of majority in your state or province of
                residence, or that you are the age of majority in your state or province of residence and you have given
                us your consent to allow any of your minor dependents to use this site.

                <br><br>
                <h3>SECTION 8 – CHANGES TO THIS PRIVACY POLICY</h3><br>
                We reserve the right to modify this privacy policy at any time, so please review it frequently. Changes
                and clarifications will take effect immediately upon their posting on the website. If we make material
                changes to this policy, we will notify you here that it has been updated, so that you are aware of what
                information we collect, how we use it, and under what circumstances, if any, we use and/or disclose
                it.<br><br>
                If our store is acquired or merged with another company, your information may be transferred to the new
                owners so that we may continue to sell products to you.

                <br><br>
                <h3>QUESTIONS AND CONTACT INFORMATION</h3><br>
                If you have any questions regarding this Privacy Policy or the practices of this Site, please contact us
                by sending an email to Hdldesigns2013@gmail.com .
                Last Updated: This Privacy Policy was last updated on 6/25/2020.

                <br><br>


            </div>
        </div>
    </div>

    <!-- Container -->
    <div class="container margin-top-50">
        <div class="four columns">

            <!-- Information -->
            <div class="widget margin-top-10">
                <div class="accordion">
                    <!-- Section 3 -->


                </div>
            </div>

            <!-- Social -->
            <div class="widget">

                <div class="clearfix"></div>
                <br>
            </div>

        </div>

        <div class="margin-top-50"></div>
    </div>
@endsection
