@extends('layouts.frontend.app')
@section('content')

    <style>b {
            font-weight: bold;
        }</style>
    <!-- Titlebar
================================================== -->
    <section class="titlebar margin-bottom-0">
        <div class="container">
            <div class="sixteen columns">
                <h2>TERMS & CONDITIONS</h2>
            </div>
        </div>
    </section>


    <!-- Content ================================================== -->

    <div class="container">
        <div class="row">
            <div class="col-sm">
                <h3><b>Welcome to HDL DESIGNS</b></h3><br>
                These terms and conditions outline the rules and regulations for the use of HDL DESIGNS
                Website.
                <br><br>
                HDL DESIGNS is located in Fort Lauderdale, Florida
                <br><br>
                By accessing this website, we assume you accept these terms and conditions in full. Do not
                continue to use HDL DESIGNS's website if you do not accept all of the terms and conditions
                stated on this page. The following terminology applies to these Terms and Conditions, Privacy
                Statement and Disclaimer Notice and any or all Agreements: "Client", "You" and "Your" refers
                to you, the person accessing this website and accepting the Company's terms and conditions.
                "The Company", "Ourselves", "We", "Our" and "Us", refers to our Company. "Party", "Parties",
                or "Us", refers to both the Client and ourselves, or either the Client or ourselves. All terms refer
                to the offer, acceptance and consideration of payment necessary to undertake the process of
                our assistance to the Client in the most appropriate manner, whether by formal meetings of a
                fixed duration, or any other means, for the express purpose of meeting the Client's needs in
                respect of provision of the Company's stated services/products, in accordance with and subject
                to, prevailing law of. Any use of the above terminology or other words in the singular, plural,
                capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to
                same. We employ the use of cookies. By using HDL DESIGNS's website you consent to the use of
                cookies in accordance with HDL DESIGNS's privacy policy. Most of the modern day interactive
                web sites use cookies to enable us to retrieve user details for each visit. Cookies are used in
                some areas of our site to enable the functionality of this area and ease of use for those people
                visiting. Some of our affiliate / advertising partners may also use cookies. Unless otherwise
                stated, HDL DESIGNS and/or it's licensors own the intellectual property rights for
                all material on HDL DESIGNS. All intellectual property rights are reserved. You may view and/or
                print pages from https://hdldesigns.com for your own personal use subject to restrictions set in
                these terms and conditions.
                <br><br>
                <b>You must not:</b><br>
                Republish material from https://hdldesigns.com <br>
                Sell, rent or sub-license material from https://hdldesigns.com <br>
                Reproduce, duplicate or copy material from https://hdldesigns.com <br>
                No use of HDL DESIGNS's logo or other artwork will be allowed for linking absent a trademark
                license agreement. Without prior approval and express written permission, you may not create
                frames around our Web pages or use other techniques that alter in any way the visual
                presentation or appearance of our Web site.
                <br><br>
                <h3>Reservation of Rights</h3> <br>
                We reserve the right at any time and in its sole discretion to request that you remove all links or
                any particular link to our Web site. You agree to immediately remove all links to our Web site
                upon such request. We also reserve the right to amend these terms and conditions and its
                linking policy at any time. By continuing to link to our Web site, you agree to be bound to and
                abide by these linking terms and conditions.
                <br><br>
                <h3>Removal of links from our website</h3> <br>
                If you find any link on our Web site or any linked web site objectionable for any reason, you
                may contact us about this. We will consider requests to remove links but will have no obligation
                to do so or to respond directly to you. Whilst we endeavor to ensure that the information on
                this website is correct, we do not warrant its completeness or accuracy; nor do we commit to
                ensuring that the website remains available or that the material on the website is kept up to
                date.
                <br><br>
                <h3>Content Liability</h3> <br>
                We shall have no responsibility or liability for any content appearing on your Web site. You
                agree to indemnify and defend us against all claims arising out of or based upon your Website.
                No link(s) may appear on any page on your Web site or within any context containing content
                or materials that may be interpreted as libelous, obscene or criminal, or which infringes,
                otherwise violates, or advocates the infringement or other violation of, any third party rights.
                <br><br>
                <h3>Disclaimer</h3> <br>
                To the maximum extent permitted by applicable law, we exclude all representations, warranties
                and conditions relating to our website and the use of this website (including, without limitation,
                any warranties implied by law in respect of satisfactory quality, fitness for purpose and/or the
                use of reasonable care and skill).
                Nothing in this disclaimer will:
                limit or exclude our or your liability for death or personal injury resulting from negligence;
                limit or exclude our or your liability for fraud or fraudulent misrepresentation;
                limit any of our or your liabilities in any way that is not permitted under applicable law; or
                exclude any of our or your liabilities that may not be excluded under applicable law.
                <br><br>
                The limitations and exclusions of liability set out in this Section and elsewhere in this disclaimer:
                (a)
                are subject to the preceding paragraph; and (b) govern all liabilities arising under the disclaimer
                or
                in relation to the subject matter of this disclaimer, including liabilities arising in contract, in tort
                (including negligence) and for breach of statutory duty. To the extent that the website and the
                information and services on the website are provided free of charge, we will not be liable for
                any loss or damage of any nature.
                <br><br>
                <h3>Typographical Errors</h3><br>
                In the event a product is listed at an incorrect price or with incorrect information due to
                typographical error or error in pricing or product information received from our suppliers,
                hdldesigns.com shall have the right to refuse or cancel any such orders and shall have the right
                to refuse or cancel any orders placed for product listed at the incorrect price. If your credit card
                has already been charged for the purchase and your order is canceled, Hdldesigns.com shall
                immediately issue a credit to your credit card account in the amount of the charge.
                <br><br>

                <h3>Pricing</h3><br>
                Any item shown on website will <b>NOT</b> be held on a layaway plan. All items must be paid in full
                unless a custom order was made.
                <br><br>
                <h3>Color variances</h3><br>
                Colors vary based on raw materials, monitor calibration, and ambient lighting. The color
                descriptions on this website or in print are representative, but may not be an exact match to
                the color of the style received.
                <br><br>
                <h3 id="customorderterms"><b>Custom order Deposit</h3><br>
                <p>By paying this deposit you are agreeing to the following:</p></b>
                <UL>
                    <li><b>- HDl designs are property of HDL Designs</b></li>
                    <li><b>- HDL Designs have the right to replicate any and all designs for custom or commercial
                            resale and is aware that similar designs may be used on all work</b></li>
                    <li><b>- Clients are not entitled to exclusivity of design element used in their custom made hat</b>
                    </li>
                    <li><b>- Allow 3-5 weeks for finish production unless discussed with owner</b></li>
                    <li><b>- All custom work is a final sale</b></li>
                    <li><b>- All deposits are non-refundable</b></li>
                    <li><b>- Deposits must be paid in full before design is created</b></li>
                    <li><b>- Aware that remainder balance quote is only an estimated and does not include
                            shipping cost</b></li>
                    <li><b>- You have 30 days to pay balance owed from invoice date or lose deposit and custom
                            order will be available for sale</b></li>
                    <li><b>- No custom design will leave HDL Designs unless paid IN FULL</b></li>
                    <li><b>- HDL Designs have the right to post photos of all designs made on social media</b></li>
                    <li><b>- Colors vary based on raw materials, monitor calibration, and ambient lighting. The
                            color descriptions on this website or in print are representative, but may not be an exact
                            match to the color of the style received</b></li>
                </ul>


            </div>
        </div>
    </div>



    <!-- Container -->
    <div class="container margin-top-50">
        <div class="four columns">

            <!-- Information -->
            <div class="widget margin-top-10">
                <div class="accordion">
                    <!-- Section 3 -->


                </div>
            </div>

            <!-- Social -->
            <div class="widget">

                <div class="clearfix"></div>
                <br>
            </div>

        </div>

        <div class="margin-top-50"></div>

    </div>
@endsection
