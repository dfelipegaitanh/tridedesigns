<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Custom Order Created</title>
</head>

<body>

    <h1>Custom Created</h1>
    <p>{{ $data['txt'] }}</p>

</body>

</html>
