<!DOCTYPE html>
<html>

<head>
    <title>Pay with Cards</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700,900|Roboto:400,700,900&display=swap" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://kit.fontawesome.com/b738a65c5f.js" crossorigin="anonymous"></script>

    <style type="text/css">
        body {
            font-family: 'Roboto', sans-serif;
        }

        .panel-title {
            display: inline;
            font-weight: bold;
        }

        .display-table {
            display: table;
        }

        .display-tr {
            display: table-row;
        }

        .display-td {
            display: table-cell;
            vertical-align: middle;
            width: 61%;
        }

        .top {
            margin-top: 65px;
        }

        .heading {
            padding: 10px;
        }

        .form-control {
            border-radius: inherit;
            height: 45px;
            border: none;
            box-shadow: 2px 0px 5px 0px #ccc;
        }

        label {
            display: inline-block;
            max-width: 100%;
            margin-bottom: 5px;
            font-weight: normal;
            font-size: 13px;
        }

        select option {
            font-size: 18px;
        }

        .slase {
            margin-top: 25px;
            font-size: 30px;
            font-weight: bold;
            margin-left: -10px;
        }

        body {
            /*background-image: url(https://www.storytel.com/images/hero-images/startpage/dk/pink/startpage_hero_desktop.jpg);*/
            background-repeat: no-repeat;
            background-size: cover;
        }

        .secMessage.inFloater {
            position: absolute;
            top: 0;
            width: 100%;
            border-radius: 4px 4px 0 0;
        }

        .secMessage {
            background: #7a0400;
            z-index: 999;
            top: 65px;
            width: 100%;
            left: 0;
            padding: 10px 5%;
            color: #fff;
            font-size: 14px;
            display: block;
            text-align: center;
        }

        .no-padding {
            padding: 0px;
        }

        ul {
            list-style: none;
            margin: 0;
            padding: 0;
        }

        ul li {
            padding: 5px 0px;
        }

        .panel.panel-default.credit-card-box {
            padding: 10px 20px;
        }

        .green {
            color: green;
        }

        .whyNeedCardInfo {
            overflow: hidden;
            display: none;
            transition: max-height .5s cubic-bezier(0, 1, 0, 1);
            border-radius: 4px;
            background: #eee;
            margin-top: 5px;
            padding: 10px;
        }

        font {
            font-weight: normal;
        }

        .form-control {
            border: 1px solid #dfdede;
            box-shadow: unset;
            border-radius: 5px;
        }

        .pr-3 {
            padding-right: 10px;
        }

        .panel-body {
            padding: 0px;
        }

        .btn-primary, .btn-primary:focus {
            outline: none !important;
            color: #fff;
            background-color: #7a0400;
            border-color: #7a0400;
        }

        .btn-primary:hover, .btn-primary:active, .btn-primary:active:focus, .btn-primary:active:hover {
            color: #fff;
            background-color: #bb2621;
            border-color: #bb2621;
        }

        .panel-body {
            margin-top: 20px;
        }

        @media (max-width: 768px) {
            .col-md-4.col-md-offset-4.no-padding {
                margin: 10px;
            }
        }
    </style>
</head>

<body>

    <div class="container top">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 no-padding">
                <div class="panel panel-default credit-card-box">

                    <div class="secMessage inFloater" id="closeShareBarW">
                        <span style="font-size: 16px; margin-right: 2px;" class="icon icon-lock"></span>
                        HDL Designs Payment Form
                    </div>
                    <br><br>

                    <div class="text-center" style="padding: 15px 0 20px;">
                        <img src="{{ asset('frontend/images/logo.png') }}" class="img-responsive" alt="logo" />
                    </div>

                    <div class="whyNeedCardInfo">
                        <p>
                            <span style="vertical-align: inherit;">
                                <span style="vertical-align: inherit;">Your card
                                    information is used for:</span>
                            </span>
                        </p>
                        <div style="margin-top: 20px;">
                            <div style="display: flex;">
                                <div><i class="fas fa-check green"></i></div>
                                <div style="margin-left: 5px;">
                                    <span style="vertical-align: inherit;">
                                        <span style="vertical-align: inherit;">Verify your account</span>
                                    </span>
                                </div>
                            </div>
                            <div style="display: flex; margin-top: 5px;">
                                <div><i class="fas fa-check green"></i></div>
                                <div style="margin-left: 5px;">
                                    <span style="vertical-align: inherit;">
                                        <span style="vertical-align: inherit;">To ensure that your subscription does not
                                            automatically end after the trial ends</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="bottomText">
                            <b>
                                <span style="vertical-align: inherit;">
                                    <span style="vertical-align: inherit;">You can cancel
                                        your subscription at any time without being charged for the trial
                                        period.</span>
                                </span>
                            </b>
                        </div>
                    </div>

                    <div class="panel-body">

                        @if (Session::has('success'))
                        <div class="alert alert-success text-center">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <p>{{ Session::get('success') }}</p>
                        </div>
                        @endif

                        <form role="form" action="{{route('stripe.payment.checkout',$order->id)}}" method="post" class="require-validation" data-cc-on-file="false" data-stripe-publishable-key="{{ env('STRIPE_KEY') }}" id="payment-form">
                            @csrf

                            <div class='form-row row'>
                                <div class='col-xs-12 form-group required'>
                                    <label class='control-label'>Card Holder Name</label>
                                    <input placeholder='Card Holder Name' class='form-control' size='4' type='text'>
                                </div>
                            </div>

                            <div class='form-row row'>
                                <div class='col-xs-12 form-group card required'>
                                    <label class='control-label'>Card Number</label>
                                    <input autocomplete='off' class='form-control card-number ccFormatMonitor' placeholder="Card Number" size='20' type='text' maxlength="20">
                                </div>
                            </div>

                            <div class='form-row row'>
                                <div class="col-md-8  ">
                                    <label class='control-label'>Expire Date</label>
                                    <input autocomplete='off' class='form-control card-expire-month' placeholder='MM/YY' size='4' id="inputExpDate" {{--                                       pattern="([0-9]{2}[/]?){2}"--}} type='text'>
                                </div>

                                <div class='col-xs-12 col-md-4 form-group cvc required'>
                                    <label class='control-label'>CVC </label>
                                    <input autocomplete='off' class='form-control card-cvc' placeholder='CVC' size='4' type='text'>
                                </div>
                            </div>
                            <div class='form-row row'>
                                <div class='col-md-12 error form-group hide'>
                                    <div class='alert-danger alert'>Please correct the errors and try
                                        again.
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <button class="btn btn-primary btn-lg btn-block" type="submit">
                                        Pay Now
                                    </button>
                                </div>
                            </div>
                        </form>
                        <br>

                    </div>
                </div>
            </div>
        </div>
    </div>


</body>

<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

<script type="text/javascript">
    var app;
    (function() {
        'use strict';
        app = {
            monthAndSlashRegex: /^\d\d \/ $/, // regex to match "MM / "
            monthRegex: /^\d\d$/, // regex to match "MM"
            el_cardNumber: '.ccFormatMonitor',
            el_expDate: '#inputExpDate',
            el_cvv: '.cvv',
            el_ccUnknown: 'cc_type_unknown',
            el_ccTypePrefix: 'cc_type_',
            el_monthSelect: '#monthSelect',
            el_yearSelect: '#yearSelect',
            cardTypes: {
                'American Express': {
                    name: 'American Express',
                    code: 'ax',
                    security: 4,
                    pattern: /^3[47]/,
                    valid_length: [15],
                    formats: {
                        length: 15,
                        format: 'xxxx xxxxxxx xxxx'
                    }
                },
                'Visa': {
                    name: 'Visa',
                    code: 'vs',
                    security: 3,
                    pattern: /^4/,
                    valid_length: [16],
                    formats: {
                        length: 16,
                        format: 'xxxx xxxx xxxx xxxx'
                    }
                },
                'Maestro': {
                    name: 'Maestro',
                    code: 'ma',
                    security: 3,
                    pattern: /^(50(18|20|38)|5612|5893|63(04|90)|67(59|6[1-3])|0604)/,
                    valid_length: [16],
                    formats: {
                        length: 16,
                        format: 'xxxx xxxx xxxx xxxx'
                    }
                },
                'Mastercard': {
                    name: 'Mastercard',
                    code: 'mc',
                    security: 3,
                    pattern: /^5[1-5]/,
                    valid_length: [16],
                    formats: {
                        length: 16,
                        format: 'xxxx xxxx xxxx xxxx'
                    }
                }
            }
        };
        app.addListeners = function() {
            $(app.el_expDate).on('keydown', function(e) {
                app.removeSlash(e);
            });
            $(app.el_expDate).on('keyup', function(e) {
                app.addSlash(e);
            });
            $(app.el_expDate).on('blur', function(e) {
                app.populateDate(e);
            });
            $(app.el_cvv + ', ' + app.el_expDate).on('keypress', function(e) {
                return e.charCode >= 48 && e.charCode <= 57;
            });
        };
        app.addSlash = function(e) {
            var isMonthEntered = app.monthRegex.exec(e.target.value);
            if (e.key >= 0 && e.key <= 9 && isMonthEntered) {
                e.target.value = e.target.value + " / ";
            }
        };
        app.removeSlash = function(e) {
            var isMonthAndSlashEntered = app.monthAndSlashRegex.exec(e.target.value);
            if (isMonthAndSlashEntered && e.key === 'Backspace') {
                e.target.value = e.target.value.slice(0, -3);
            }
        };
        app.populateDate = function(e) {
            var month, year;
            if (e.target.value.length == 7) {
                month = parseInt(e.target.value.slice(0, -5));
                year = "20" + e.target.value.slice(5);
                if (app.checkMonth(month)) {
                    $(app.el_monthSelect).val(month);
                } else {
                    $(app.el_monthSelect).val(0);
                }
                if (app.checkYear(year)) {
                    $(app.el_yearSelect).val(year);
                } else {
                    $(app.el_yearSelect).val(0);
                }
            }
        };
        app.checkMonth = function(month) {
            if (month <= 12) {
                var monthSelectOptions = app.getSelectOptions($(app.el_monthSelect));
                month = month.toString();
                if (monthSelectOptions.includes(month)) {
                    return true;
                }
            }
        };
        app.checkYear = function(year) {
            var yearSelectOptions = app.getSelectOptions($(app.el_yearSelect));
            if (yearSelectOptions.includes(year)) {
                return true;
            }
        };
        app.getSelectOptions = function(select) {
            var options = select.find('option');
            var optionValues = [];
            for (var i = 0; i < options.length; i++) {
                optionValues[i] = options[i].value;
            }
            return optionValues;
        };
        app.setMaxLength = function($elem, length) {
            if ($elem.length && app.isInteger(length)) {
                $elem.attr('maxlength', length);
            } else if ($elem.length) {
                $elem.attr('maxlength', '');
            }
        };
        app.isInteger = function(x) {
            return (typeof x === 'number') && (x % 1 === 0);
        };
        app.createExpDateField = function() {
            $(app.el_monthSelect + ', ' + app.el_yearSelect).hide();
            $(app.el_monthSelect).parent().prepend('<input type="text" class="ccFormatMonitor">');
        };
        app.isValidLength = function(cc_num, card_type) {
            for (var i in card_type.valid_length) {
                if (cc_num.length <= card_type.valid_length[i]) {
                    return true;
                }
            }
            return false;
        };
        app.getCardType = function(cc_num) {
            for (var i in app.cardTypes) {
                var card_type = app.cardTypes[i];
                if (cc_num.match(card_type.pattern) && app.isValidLength(cc_num, card_type)) {
                    return card_type;
                }
            }
        };
        app.getCardFormatString = function(cc_num, card_type) {
            for (var i in card_type.formats) {
                var format = card_type.formats[i];
                if (cc_num.length <= format.length) {
                    return format;
                }
            }
        };
        app.formatCardNumber = function(cc_num, card_type) {
            var numAppendedChars = 0;
            var formattedNumber = '';
            var cardFormatIndex = '';
            if (!card_type) {
                return cc_num;
            }
            var cardFormatString = app.getCardFormatString(cc_num, card_type);
            for (var i = 0; i < cc_num.length; i++) {
                cardFormatIndex = i + numAppendedChars;
                if (!cardFormatString || cardFormatIndex >= cardFormatString.length) {
                    return cc_num;
                }
                if (cardFormatString.charAt(cardFormatIndex) !== 'x') {
                    numAppendedChars++;
                    formattedNumber += cardFormatString.charAt(cardFormatIndex) + cc_num.charAt(i);
                } else {
                    formattedNumber += cc_num.charAt(i);
                }
            }
            return formattedNumber;
        };
        app.monitorCcFormat = function($elem) {
            var cc_num = $elem.val().replace(/\D/g, '');
            var card_type = app.getCardType(cc_num);
            $elem.val(app.formatCardNumber(cc_num, card_type));
            app.addCardClassIdentifier($elem, card_type);
        };
        app.addCardClassIdentifier = function($elem, card_type) {
            var classIdentifier = app.el_ccUnknown;
            if (card_type) {
                classIdentifier = app.el_ccTypePrefix + card_type.code;
                app.setMaxLength($(app.el_cvv), card_type.security);
            } else {
                app.setMaxLength($(app.el_cvv));
            }
            if (!$elem.hasClass(classIdentifier)) {
                var classes = '';
                for (var i in app.cardTypes) {
                    classes += app.el_ccTypePrefix + app.cardTypes[i].code + ' ';
                }
                $elem.removeClass(classes + app.el_ccUnknown);
                $elem.addClass(classIdentifier);
            }
        };
        app.init = function() {
            $(document).find(app.el_cardNumber).each(function() {
                var $elem = $(this);
                if ($elem.is('input')) {
                    $elem.on('input', function() {
                        app.monitorCcFormat($elem);
                    });
                }
            });
            app.addListeners();
        }();
    })();
    $(".whyNeedCardButton").click(function() {
        $(".whyNeedCardInfo").toggle('fast');
    })
    $(function() {
        var $form = $(".require-validation");
        $('form.require-validation').bind('submit', function(e) {
            var $form = $(".require-validation"),
                inputSelector = ['input[type=email]', 'input[type=password]',
                    'input[type=text]', 'input[type=file]',
                    'textarea'
                ].join(', '),
                $inputs = $form.find('.required').find(inputSelector),
                $errorMessage = $form.find('div.error'),
                valid = true;
            $errorMessage.addClass('hide');
            $('.has-error').removeClass('has-error');
            $inputs.each(function(i, el) {
                var $input = $(el);
                if ($input.val() === '') {
                    $input.parent().addClass('has-error');
                    $errorMessage.removeClass('hide');
                    e.preventDefault();
                }
            });
            if (!$form.data('cc-on-file')) {
                e.preventDefault();
                Stripe.setPublishableKey($form.data('stripe-publishable-key'));
                let expgroup = $('.card-expire-month').val()
                let expArray = expgroup.split('/');
                let expmm = (expArray[0]);
                let expyy = (expArray[1]);
                Stripe.createToken({
                    number: $('.card-number').val(),
                    cvc: $('.card-cvc').val(),
                    exp_month: parseInt(expmm),
                    exp_year: parseInt(expyy)
                }, stripeResponseHandler);
            }
        });

        function stripeResponseHandler(status, response) {
            if (response.error) {
                $('.error')
                    .removeClass('hide')
                    .find('.alert')
                    .text(response.error.message);
            } else {
                // token contains id, last4, and card type
                let token = response['id'];
                // insert the token into the form so it gets submitted to the server
                $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
                $form.get(0).submit();
            }
        }
    });
</script>

</html>
