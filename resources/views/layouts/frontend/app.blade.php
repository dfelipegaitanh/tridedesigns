<!DOCTYPE html>
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> <!--<![endif]-->
<head>

    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <title>HDL Designs</title>

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/colors/red1.css') }}" id="colors">
    <script src="{{ asset('frontend/instashow/elfsight-instagram-feed.js') }}"></script>
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
    <link rel="stylesheet" href="{{ asset('frontend/css/modal.css') }}">
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <style>
        .preloader {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('{{ asset('frontend/images/preloader.gif') }}') 50% 50% no-repeat rgb(255, 255, 255);
            opacity: 1;
        }
    </style>
</head>
<div class="preloader" id="preloaders"></div>
<body class="fullwidth">
<div id="wrapper">


    <!-- Top Bar
    ================================================== -->


    <div class="clearfix"></div>


    <!-- Header
    ================================================== -->
    <div class="container" style="display: flex; justify-content: center;">


        <!-- Logo -->
        <div class="two columns" style="width: auto !important">
            <div id="logo">
                <h1><a href="{{ url('/') }}"><img src="{{ asset('frontend/images/logo.png') }}" alt="HDL Designs Logo"/></a>
                </h1>
            </div>
        </div>

    </div>

</div>


<!-- Navigation
================================================== -->
<div class="container">
    <div class="sixteen columns">

        <a href="#menu" class="menu-trigger"><i class="fa fa-bars"></i> Menu</a>

        <nav id="navigation">
            <ul class="menu" id="responsive">
                <li><a href="{{ url('/') }}" class="homepage  {{ Request::path() === '' ? 'current' : ''}}">HOME</a>
                </li>
                <li class="dropdown">
                    <a href="#">SHOP</a>
                    <ul>
                        @php
                            $collection = App\Collection::orderBy('id', 'DESC')->get()
                        @endphp
                        @foreach ($collection as $item)
                            <li><a href="{{ url('shop/'.$item->slug) }}">{{ ucfirst($item->name) }}</a></li>
                        @endforeach
                    </ul>
                </li>
                <li class="{{ Request::path() === '/gallery' ? 'current' : ''}}"><a
                        href="{{ url('/gallery') }}">Gallery</a></li>
                <li class="{{ Request::path() === '/blog' ? 'current' : ''}}"><a href="{{ url('/blog') }}">BLOG</a></li>
                <li class="{{ Request::path() === '/about' ? 'current' : ''}}"><a href="{{ url('/about') }}">ABOUT</a>
                </li>
                <li><a href="{{ url('/contact') }}">CONTACT</a></li>
                <li class="customorderbtn" style="float: right !important; background-color: #7a0400;"
                    onclick="document.getElementById('id01').style.display='block'"><a href="#">CUSTOM ORDER</a></li>
            </ul>
        </nav>
    </div>
</div>

@yield('content')

<!-- Footer
================================================== -->
<div id="footer" style="margin-left: calc(50% - 50vw);margin-right: calc(50% - 50vw);">

    <!-- Container -->
    <div class="container" style="display: flex; justify-content: center;">

        <div class="sixteen columns" style="width: auto !important">

            <!-- Headline -->
            <h3 class="headline footer">Customer Service</h3>
            <span class="line"></span>
            <div class="clearfix"></div>

            <ul class="footer-links">
                <li><a href="{{ route('returns') }}">Delivery & Returns</a></li>
                <li><a href="{{ route('privacy') }}">Privacy Policy</a></li>
                <li><a href="{{ route('terms') }}">Terms & Conditions</a></li>
                <li><a href="{{ route('faqs') }}">FAQS</a></li>
                <li><a href="{{ url('contact') }}">Contact Us</a></li>
            </ul>

        </div>

        <div class="two columns" style="width: auto !important">
            <img src="{{ asset('frontend/images/logo-footer.png') }}" alt="" class="margin-top-10"/>

        </div>


    </div>
    <!-- Container / End -->

</div>
<!-- Footer / End -->
<!-- custom order -->
<div id="id01" class="w3-modal">
    <div class="w3-modal-content w3-card-4 w3-animate-zoom">   <span
            onclick="document.getElementById('id01').style.display='none'"
            class="w3-button w3-blue w3-xlarge w3-display-topright">&times;</span>
        <div class="modalinner">
            <img src="{{ asset('frontend/images/logo.png') }}" alt="HDL Designs Logo"/>

            <span id="mainForm">
				<h2>Custom Order Form</h2>
				<form method="post" id="coForm">
					@csrf
					<input type="text" name="name" class="halff mar5" placeholder="NAME">
					<input type="text" name="number" class="halff" placeholder="CONTACT NUMBER">
					<input type="text" name="email" class="halff mar5" placeholder="EMAIL">
					<input type="text" name="color" class="halff" placeholder="COLOR">
					<input type="text" name="headsize" class="halff mar5" placeholder="HEAD SIZE">
					<input type="text" name="hatpin" class="halff" placeholder="HAT PIN (ADDITIONAL CHARGE)">
					<textarea name="additional" id="" cols="45" rows="5" class="fulll"
                              placeholder="ADDITIONAL INFO"></textarea>
					<button class="formBtn full" id="subord">SUBMIT CUSTOM ORDER</button>
					<p>by submitting this form, you agree to the <a href="terms">terms & conditions</a> on custom orders. A depost will be required for all hdl custom orders.</p>
					<div class="clearfix"></div>

					<div class="notification error print-error-msg" style="display:none;text-align:left;">
						<ul></ul>
					</div>
				</form>
			</span>
            <div class="full thanks" style="display:none;">
                <h2>Thank You</h2>
                <h4>your custom order has been RECEIVED. we will REACH OUT TO YOU AS SOON AS POSSIBLE.</h4>
                <button class="formBtn full" onclick="document.getElementById('id01').style.display='none'">EXIT
                </button>
            </div>
        </div>
    </div>
</div>
<!-- custom order closed -->

<!-- Footer Bottom / Start -->
<div id="footer-bottom" style="margin-left: calc(50% - 50vw);margin-right: calc(50% - 50vw);">

    <!-- Container -->
    <div class="container">

        <div class="sixteen columns" style="text-align: center;"><span>Copyright &copy; HDL Designs <script> var dteNow = new Date();
                    var intYear = dteNow.getFullYear();
                    document.write(intYear);</script> </span>. Web Design by <a href="https://tridedesigns.com/">Tride
                Designs</a></div>

    </div>
    <!-- Container / End -->

</div>
<!-- Footer Bottom / End -->

<!-- Back To Top Button -->
<div id="backtotop" style="width: 45px;"><a href="#"></a></div>


<!-- Java Script
================================================== -->
<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="{{ asset('frontend/scripts/jquery.jpanelmenu.js') }}"></script>
<script src="{{ asset('frontend/scripts/jquery.themepunch.plugins.min.js') }}"></script>
<script src="{{ asset('frontend/scripts/jquery.themepunch.revolution.min.js') }}"></script>
<script src="{{ asset('frontend/scripts/jquery.themepunch.showbizpro.min.js') }}"></script>
<script src="{{ asset('frontend/scripts/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('frontend/scripts/hoverIntent.js') }}"></script>
<script src="{{ asset('frontend/scripts/superfish.js') }}"></script>
<script src="{{ asset('frontend/scripts/jquery.pureparallax.js') }}"></script>
<script src="{{ asset('frontend/scripts/jquery.pricefilter.js') }}"></script>
<script src="{{ asset('frontend/scripts/jquery.selectric.min.js') }}"></script>
<script src="{{ asset('frontend/scripts/jquery.royalslider.min.js') }}"></script>
<script src="{{ asset('frontend/scripts/SelectBox.js') }}"></script>
<script src="{{ asset('frontend/scripts/modernizr.custom.js') }}"></script>
<script src="{{ asset('frontend/scripts/waypoints.min.js') }}"></script>
<script src="{{ asset('frontend/scripts/jquery.flexslider-min.js') }}"></script>
<script src="{{ asset('frontend/scripts/jquery.counterup.min.js') }}"></script>
<script src="{{ asset('frontend/scripts/jquery.tooltips.min.js') }}"></script>
<script src="{{ asset('frontend/scripts/jquery.isotope.min.js') }}"></script>
<script src="{{ asset('frontend/scripts/puregrid.js') }}"></script>
<script src="{{ asset('frontend/scripts/stacktable.js') }}"></script>
<script src="{{ asset('frontend/scripts/custom.js') }}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>


@yield('footer-script')


<script>
    $(window).load(function () {
        $("#preloaders").fadeOut(1000);
    });
    var _0x2c6f = ['ajax', 'preventDefault', 'html', '#mainForm', 'append', '.print-error-msg', 'css', 'hide', 'serialize', 'click', '<li>', '</li>', '#subord', 'POST', 'errors', 'each', 'show', '.thanks', 'display'];
    (function (_0x32003d, _0x2c6fd4) {
        var _0x112bb0 = function (_0x1edffd) {
            while (--_0x1edffd) {
                _0x32003d['push'](_0x32003d['shift']());
            }
        };
        _0x112bb0(++_0x2c6fd4);
    }(_0x2c6f, 0x71));
    var _0x112b = function (_0x32003d, _0x2c6fd4) {
        _0x32003d = _0x32003d - 0x0;
        var _0x112bb0 = _0x2c6f[_0x32003d];
        return _0x112bb0;
    };
    $(_0x112b('0xd'))[_0x112b('0xa')](function (_0x3460c1) {
        _0x3460c1[_0x112b('0x2')]();
        $[_0x112b('0x1')]({
            'url': '/corder',
            'method': _0x112b('0xe'),
            'data': $('#coForm')[_0x112b('0x9')](),
            'success': function (_0x5ca20c) {
                if (_0x5ca20c == 0x1) {
                    $(_0x112b('0x4'))[_0x112b('0x8')]();
                    $(_0x112b('0x12'))[_0x112b('0x11')]();
                    $(_0x112b('0x12'))['slideDown']();
                } else {
                    printErrorMsg(_0x5ca20c[_0x112b('0xf')]);
                }
            }
        });
    });

    function printErrorMsg(_0x15c92b) {
        $(_0x112b('0x6'))['find']('ul')[_0x112b('0x3')]('');
        $(_0x112b('0x6'))[_0x112b('0x7')](_0x112b('0x0'), 'block');
        $[_0x112b('0x10')](_0x15c92b, function (_0x42cf7b, _0x22a8b2) {
            $(_0x112b('0x6'))['find']('ul')[_0x112b('0x5')](_0x112b('0xb') + _0x22a8b2 + _0x112b('0xc'));
        });
    }

        @if(Session::has('messege'))
    var type = "{{Session::get('alert-type','info')}}"
    switch (type) {
        case 'info':
            toastr.info("{{ Session::get('messege') }}");
            break;
        case 'success':
            toastr.success("{{ Session::get('messege') }}");
            break;
        case 'warning':
            toastr.warning("{{ Session::get('messege') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('messege') }}");
            break;
    }
        @endif
    var _0x430b = ['city', 'click', 'classList', 'display', 'tablink', 'w3-light-grey', 'block', 'getElementsByClassName', 'none', 'getElementById', 'currentTarget', 'style', 'add'];
    (function (_0x283590, _0x430bd5) {
        var _0x276dc2 = function (_0x21da76) {
            while (--_0x21da76) {
                _0x283590['push'](_0x283590['shift']());
            }
        };
        _0x276dc2(++_0x430bd5);
    }(_0x430b, 0x10d));
    var _0x276d = function (_0x283590, _0x430bd5) {
        _0x283590 = _0x283590 - 0x0;
        var _0x276dc2 = _0x430b[_0x283590];
        return _0x276dc2;
    };
    document[_0x276d('0xb')](_0x276d('0x8'))[0x0][_0x276d('0x5')]();

    function openCity(_0x2f62c6, _0x3d79e9) {
        var _0x9baa54, _0x560478, _0x3945a3;
        _0x560478 = document[_0x276d('0xb')](_0x276d('0x4'));
        for (_0x9baa54 = 0x0; _0x9baa54 < _0x560478['length']; _0x9baa54++) {
            _0x560478[_0x9baa54][_0x276d('0x2')][_0x276d('0x7')] = _0x276d('0xc');
        }
        _0x3945a3 = document[_0x276d('0xb')]('tablink');
        for (_0x9baa54 = 0x0; _0x9baa54 < _0x560478['length']; _0x9baa54++) {
            _0x3945a3[_0x9baa54][_0x276d('0x6')]['remove']('w3-light-grey');
        }
        document[_0x276d('0x0')](_0x3d79e9)['style'][_0x276d('0x7')] = _0x276d('0xa');
        _0x2f62c6[_0x276d('0x1')]['classList'][_0x276d('0x3')](_0x276d('0x9'));
    }


</script>

</body>
</html>
