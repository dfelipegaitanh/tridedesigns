@extends('layouts.admin.layout')
@section('content')

    <style>
        .card.card-statistics {
            background: linear-gradient(85deg, #06b76b, #f5a623);
            color: #ffffff;
        }
    </style>
    <div class="main-panel" style="width: 100% !important;">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4>All Order</h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Order id</th>
                                                        <th>customer name</th>
                                                        <th>Amount</th>
                                                        <th>email</th>
                                                        <th>Product name</th>
                                                        <th>Status</th>
                                                        <th>Ordered At</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @php
                                                        $i=0;
                                                    @endphp
                                                    @foreach ($orders as $item)
                                                        @php
                                                            $i++;
                                                        @endphp
                                                        <tr>
                                                            <td>{{ $i }}</td>
                                                            <td>{{ $item->id }}</td>
                                                            <td>{{ $item->name }}</td>
                                                            <td>${{ $item->total }}</td>
                                                            <td>{{ $item->email }}</td>
                                                            <td>{{ $item->product->name }}</td>
                                                            <td>
                                                                
                                                                 @if($item->orderstatus == 'p')
                                                                    <i class="fa fa-history text-warning" title="Pending"></i>
                                                                    @elseif($item->orderstatus == 's')
                                                                    <i class="fas fa-shipping-fast text-primary" title="Item has shipped"></i>  
                                                                    @elseif($item->orderstatus == 'c')
                                                                    <i class="fa fa-check text-success" title="Item Complete"></i>  
                                                                    @elseif($item->orderstatus == 'r')
                                                                    <i class="fa fa-arrow-left text-danger" title="Refund Issued"></i>  
                                                                    @else
                                                                    <i class="fa fa-times text-danger" title="Undefined"></i> 
                                                                    @endif
                                                                                                
                                                            </td>
                                                            <td>{{ $item->created_at->format('d F, Y H:i:s') }}</td>

                                                            <td>
                                                                <div class="btn-group">
                                                                    <button type="button"
                                                                            class="btn btn-primary dropdown-toggle btn-sm rounded-0"
                                                                            data-toggle="dropdown" aria-haspopup="true"
                                                                            aria-expanded="false">
                                                                        select
                                                                    </button>
                                                                    <div class="dropdown-menu dropdown-menu-right">
                                                                        <a href="{{ url('admin/orders/view/'.$item->id) }}"
                                                                           class="dropdown-item">View</a>
                                                                           
                                                                        <a href="{{ url('admin/orders/update/'.$item->id.'/s') }}"
                                                                           class="dropdown-item">Mark as Shipped</a>
                                                                           
                                                                        <a href="{{ url('admin/orders/update/'.$item->id.'/c') }}"
                                                                           class="dropdown-item">Mark as Complete</a>
                                                                           
                                                                        <a href="{{ url('admin/orders/update/'.$item->id.'/r') }}"
                                                                           class="dropdown-item">Mark as Refund</a>
                                                                           
                                                                        <a href="{{ url('admin/orders/delete/'.$item->id) }}"
                                                                           class="dropdown-item" id="delete">Delete</a>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                    </tbody>
                                                </table>
                                                {{ $orders->links() }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        


            </div>
        </div>

    </div>
@endsection

@section('footer')

@endsection
