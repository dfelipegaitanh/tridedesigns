@extends('layouts.admin.layout')
@section('content')

    <style>
        .card.card-statistics {
            background: linear-gradient(85deg, #06b76b, #f5a623);
            color: #ffffff;
        }
    </style>
    <div class="main-panel" style="width: 100% !important;">
        <div class="content-wrapper">
           <div class="row">
               <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <h4>All Blogs</h4>
                        <a href="{{ url('admin/blogs/create') }}" class="btn btn-primary badge-pill" >Add Blog</a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Image</th>
                                        <th>Comments</th>
                                        <th>Actions</th>    
                                    </tr>       
                                </thead> 
                                <tbody>
                                    @foreach ($blogs as $blog)
                                    @php
                                     $commentscount = App\Comment::where('blog_id','=',$blog->id)->count();	
                                    @endphp

                            <tr>
                                <td>{{ $blog->id }}</td>
                                <td><a href="#" style="color: #000000;text-decoration: none">{{ $blog->title }}</a></td>

                                <td>
                                    <img src="{{ Storage::url($blog->freature_image) }}" alt="image" style="width: 50px;height: 50px;">
                                </td>
                                <td>{{$commentscount}}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary dropdown-toggle rounded-0 btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        select
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                        <a href="{{ url('admin/blog/edit/'.$blog->id) }}" class="dropdown-item" type="button">Edit</a>
                                        <a href="{{ url('admin/blog/delete/'.$blog->id) }}" class="dropdown-item" type="button">delete</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                         @endforeach
                                    
                                </tbody>
                            </table> 
                            {{ $blogs->links() }}   
                        </div>   
                    </div>
                </div>
               </div>
             
             
           </div>
        </div>

    </div>
@endsection

@section('footer')

@endsection
