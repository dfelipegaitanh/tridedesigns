@extends('layouts.admin.layout')
@section('content')

    <style>
        .card.card-statistics {
            background: linear-gradient(85deg, #06b76b, #f5a623);
            color: #ffffff;
        }
    </style>
    <div class="main-panel" style="width: 100% !important;">
        <div class="content-wrapper">
           <div class="row">
               <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Promotions</h4>
                    </div>
                    <div class="card-body">
                      <div class="row">
                       <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Add Promotion</h4>
                                </div>
                                <div class="card-body">
                                    <form action="{{ url('admin/promotion/store/') }}" method="POST" >
                                        @csrf
                                        <div class="form-group">
                                            <label>Promotion Code</label>
                                            <input type="text" class="form-control" name="name">
                                            @error('name')
                                            <br>
                                                <span class="text-danger" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                            <br>
                                            <label>Discount <small>%</small></label>
                                            <input type="text" class="form-control" name="discount">
                                            @error('discount')
                                            <br>
                                                <span class="text-danger" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                            <br>
                                            <label>Collection</label>
                                              @php
                                                    $collectons = App\Collection::get();
                                                @endphp
                                              <select name="cid" class="form-control">  
                                              <option selected value="0">Apply to All</option>
                                                @foreach ($collectons as $collection_item)
                                                    <option value="{{ $collection_item->id }}">{{ $collection_item->name }}</option>
                                                @endforeach
                                        </select>
                                        </div>
                                        <div class="form-group float-right">
                                            <input type="submit" class="btn btn-primary rounded-0" value="Save">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>  
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">
                                <h4>All Promotions</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                 <th>Code</th>
                                                 <th>Applies To</th>
                                                 <th>Discount</th>
                                                <th>Actions</th>    
                                            </tr>       
                                        </thead> 
                                        <tbody>
                                            @php
                                                $i=0;
                                            @endphp
                                            @foreach ($promotion as $item)
                                            @php
                                                $i++;
                                            @endphp
                                            <tr>
                                                <td>{{ $i }}</td>
                                                <td>{{ $item->name }}</td>
                                                <td>
                                                    @if($item->applyto == 0)
                                                    {{'All Collections'}}
                                                    @else
                                                        @php
                                                        $collection = App\Collection::find($item->applyto);
                                                        @endphp
                                                        {{$collection->name}}
                                                    @endif
                                                </td>
                                                <td>{{ $item->percentage }}%</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-primary dropdown-toggle btn-sm rounded-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        select
                                                        </button>
                                                        <div class="dropdown-menu dropdown-menu-right">
                                                        <a href="{{ url('admin/promotion/delete/'.$item->id) }}" class="dropdown-item" id="delete">Delete</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                           

                                        </tbody>
                                    </table>    
                                </div>      
                            </div>    
                        </div>          
                    </div>  
                    </div>
                </div>
               </div>
                        </div> 
             
             
           </div>
        </div>

    </div>
@endsection

@section('footer')

@endsection