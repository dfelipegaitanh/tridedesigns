@extends('layouts.admin.layout')
@section('content')

    <style>
        .card.card-statistics {
            background: linear-gradient(85deg, #06b76b, #f5a623);
            color: #ffffff;
        }
    </style>
    <div class="main-panel" style="width: 100% !important;">
        <div class="content-wrapper">
           <div class="row">
               <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <h4>All Custom Orders</h4>
                       
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Number</th>
                                        <th>Color</th>
                                        <th>Size</th>
                                        <th>HatPin</th>
                                        <th>Additional</th>    
                                        <th>Status</th>  
                                        <th>Date</th>  
                                        <th>Action</th>    
                                    </tr>       
                                </thead> 
                                <tbody>                                                                                                   
                            @foreach($customs as $custom)
                             <tr>
                                <td>{{$custom->id }}</td>
                                <td>{{$custom->name }}</td>
                                <td>{{$custom->email}}</td>
                                <td>{{$custom->number}}</td>
                                <td>{{$custom->color}}</td>
                                <td>{{$custom->size}}</td>
                                <td>{{$custom->hatpin}}</td>
                                <td>{{$custom->additioninfo}}</td>            
                                <td>
                                    @if($custom->status == 0)
                                    <i class="fa fa-history text-warning"></i>
                                    @elseif($custom->status == 1)
                                    <i class="fa fa-check text-success"></i>                                   
                                    @endif
                                </td>
                                <td>{{$custom->created_at}}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary dropdown-toggle rounded-0 btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        select
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                        @if ($custom->status == 0)                                            
                                            <a href="{{ url('admin/custom-orders/update/'.$custom->id) }}" class="dropdown-item" type="button">Approve</a>                              
                                        @endif
                                         <a href="{{ url('admin/custom-orders/delete/'.$custom->id) }}" class="dropdown-item" type="button" id="delete">
                                             Delete</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                         @endforeach
                                    
                                </tbody>
                            </table> 
                           
                        </div>   
                    </div>
                </div>
                {{ $customs->links() }}   
               </div>
             
             
           </div>
        </div>

    </div>
@endsection

@section('footer')

@endsection