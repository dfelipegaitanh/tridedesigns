@extends('layouts.admin.layout')
<style>
.bootstrap-tagsinput .tag {
    margin-right: 2px;
    color: #fff;
    background: #9883eb;
    border-radius: 3px;
    padding: 2px 2px 2px 7px;
}
</style>
@section('content')

    <style>
        .card.card-statistics {
            background: linear-gradient(85deg, #06b76b, #f5a623);
            color: #ffffff;
        }
    </style>
    
    <div class="main-panel" style="width: 100% !important;">
        <div class="content-wrapper">
           <div class="row">
               <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Update Collection</h4>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                            
                        <form action="{{ url('admin/collection/update/'.$collection->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                              <label>Collection Name</label>
                              <input type="text" class="form-control" name="name" placeholder="Product name" value="{{ $collection->name }}">
                            </div>
                  
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label>Featured image</label>
                                  <input type="file" class="form-control-file" name="freature_image">
                                </div>
                              </div>
                                
                                
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label>Image Preview</label><br>
                                  @if($collection->image)
                                    <img src="{{ Storage::url($collection->image) }}" alt="image" style="width: 120px;height: 120px;">
                                  @else
                                  <h4>No Featured image is selected</h4>
                                  @endif
                                </div>
                              </div>
                                
                            </div>
                            
          
                            <div class="form-group float-right">
                                <button type="submit" class="btn btn-success rounded-0">Save</button>
                            </div>

                        </form>
                    </div>
                </div>
               </div>
             
             
           </div>
        </div>

    </div>
@endsection

@section('footer')

      <script>
        $(function () {
    
            $('.input-images-1').imageUploader();
    
            let preloaded = [
                // {id: 1, src: 'https://picsum.photos/500/500?random=1'},
                // {id: 2, src: 'https://picsum.photos/500/500?random=2'},
                // {id: 3, src: 'https://picsum.photos/500/500?random=3'},
                // {id: 4, src: 'https://picsum.photos/500/500?random=4'},
                // {id: 5, src: 'https://picsum.photos/500/500?random=5'},
                // {id: 6, src: 'https://picsum.photos/500/500?random=6'},
            ];
    
            $('.input-images-2').imageUploader({
                preloaded: preloaded,
                imagesInputName: 'photos',
                preloadedInputName: 'old'
            });
    
          
            // Input and label handler
            $('input').on('focus', function () {
                $(this).parent().find('label').addClass('active')
            }).on('blur', function () {
                if ($(this).val() == '') {
                    $(this).parent().find('label').removeClass('active');
                }
            });
    
            // Sticky menu
            let $nav = $('nav'),
                $header = $('header'),
                offset = 4 * parseFloat($('body').css('font-size')),
                scrollTop = $(this).scrollTop();
    
            // Initial verification
            setNav();
    
            // Bind scroll
            $(window).on('scroll', function () {
                scrollTop = $(this).scrollTop();
                // Update nav
                setNav();
            });
    
            function setNav() {
                if (scrollTop > $header.outerHeight()) {
                    $nav.css({position: 'fixed', 'top': offset});
                } else {
                    $nav.css({position: '', 'top': ''});
                }
            }
        });
    </script>
@endsection