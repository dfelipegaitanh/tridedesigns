@extends('layouts.admin.layout')
@section('content')

    <style>
        .card.card-statistics {
            background: linear-gradient(85deg, #06b76b, #f5a623);
            color: #ffffff;
        }
    </style>
    <div class="main-panel" style="width: 100% !important;">
        <div class="content-wrapper">
           <div class="row">
               <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <h4>All Comments</h4>
                       
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Avatar</th>
                                        <th width="10%">P-Title</th>
                                        <th width="10%">Name</th>
                                        <th>Email</th>
                                        <th width="35%">Comment</th>
                                        <th>Status</th>
                                        <th>Date</th>
                                        <th>Actions</th>    
                                    </tr>       
                                </thead> 
                                <tbody>                                                         
                                    @foreach ($comments as $comment)                                        
                                    @php                                                                                                       		                                
                                     $blog = App\Blog::find($comment->blog_id);
                                    @endphp                
                             <tr>
                                <td>{{ $comment->id }}</td>
                                <td>  @if($comment->avatar)
            					    <img src="{{ Storage::url($comment->avatar) }}" width="150px">
            					    @else
            					    <img src="http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&amp;s=70" width="150px">
            					    @endif
            					    </td>
                                <td><a href="#" style="color: #000000;text-decoration: none">{{ $blog->title }}</a></td>
                                <td>{{$comment->name}}</td>
                                <td>{{$comment->email}}</td>
                                <td>{{$comment->comment}}</td>
                                <td>
                                    @if($comment->status == 'p')
                                    <i class="fa fa-history text-warning"></i>
                                    @elseif($comment->status == 'a')
                                    <i class="fa fa-check text-success"></i>
                                    @elseif($comment->status == 'r')
                                    <i class="fa fa-times text-danger"></i>
                                    @endif
                                </td>
                                <td>{{$comment->created_at}}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary dropdown-toggle rounded-0 btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        select
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                        @if ($comment->status == 'p')                                            
                                            <a href="{{ url('admin/comments/update/'.$comment->id) }}" class="dropdown-item" type="button">Approve</a>                              
                                        @endif
                                        <a href="{{ url('admin/comment/delete/'.$comment->id) }}" class="dropdown-item" type="button">delete</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                         @endforeach
                                    
                                </tbody>
                            </table> 
                           
                        </div>   
                    </div>
                </div>
                {{ $comments->links() }}   
               </div>
             
             
           </div>
        </div>

    </div>
@endsection

@section('footer')

@endsection