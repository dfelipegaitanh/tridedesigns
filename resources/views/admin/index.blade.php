@extends('layouts.admin.layout')
@section('content')

    <style>
        .card.card-statistics {
            background: linear-gradient(85deg, #06b76b, #f5a623);
            color: #ffffff;
        }
    </style>
    <div class="main-panel" style="width: 100% !important;">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">
                    Dashboards
                </h3>
            </div>
            <div class="row grid-margin">
                <div class="col-12">
                    <div class="card card-statistics">
                        <div class="card-body">
                            <div class="d-flex flex-column flex-md-row align-items-center justify-content-between">

                                <div class="statistics-item">
                                    <p>
                                        <i class="icon-sm fab fa-trello menu-icon mr-2"></i>
                                        Total Collections
                                    </p>
                                     @php
                                        $collections = 0;
                        				$collections = App\Collection::count();
                        				@endphp
                                    <h2>{{$collections}}</h2>
                                </div>
                                <div class="statistics-item">
                                    <p>
                                        <i class="icon-sm fab fa-wpforms menu-icon mr-2"></i>
                                        Total product
                                    </p>
                                     @php
                                        $products = 0;
                        				$products = App\Product::count();
                        				@endphp
                                    <h2>{{$products}}</h2>

                                </div>
                                <div class="statistics-item">
                                    <p>
                                        <i class="icon-sm fas fa-hourglass-half mr-2"></i>
                                        Total sell
                                    </p>
                                     @php
                                        $sold = 0;
                        				$sold = App\Order::where('status',1)->count();
                        				@endphp
                                    <h2>{{$sold}}</h2>

                                </div>

                                <div class="statistics-item">
                                    <p>
                                        <i class="icon-sm fas fa-users mr-2"></i>
                                        Total Subscribers
                                    </p>
                                    <h2>#</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <div class="row grid-margin">
                <div class="col-12">
                    <div class="card card-statistics">
                        <div class="card-body">
                            <div class="d-flex flex-column flex-md-row align-items-center justify-content-between">
                                    
                                    
                                <div class="statistics-item">
                                    <p>
                                        <i class="icon-sm fas fa-users mr-2"></i>
                                        @php
                                        $blogs = 0;
                        				$blogs = App\Blog::count();
                        				@endphp
                                        Total Blog Posts
                                    </p>
                                    <h2>{{$blogs}}</h2>
                                </div>
                                    
                                <div class="statistics-item">
                                    <p>
                                        <i class="icon-sm fa fa-comments menu-icon mr-2"></i>
                                        @php
                                        $comments = 0;
                        				$comments = App\Comment::count();
                        				@endphp
                                        Total Comments
                                    </p>
                                    <h2>{{$comments}}</h2>
                                </div>
                                <div class="statistics-item">
                                    <p>
                                        <i class="icon-sm fas fa-hourglass-half mr-2"></i>
                                        @php
                                        $pcomments = 0;
                        				$pcomments = App\Comment::where('status','=','p')->count()
                        				@endphp
                                        Pending Comments
                                    </p>
                                    <h2>{{$pcomments}}</h2>

                                </div>
                                <div class="statistics-item">
                                    <p>
                                        <i class="icon-sm fa fa-check menu-icon mr-2"></i>
                                        @php
                                        $acomments = 0;
                        				$acomments = App\Comment::where('status','=','a')->count()
                        				@endphp
                                        Approved Comments
                                    </p>
                                    <h2>{{$acomments}}</h2>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            {{-- <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Recent Activities</h4>
                            <div class="mt-5">
                                <div class="timeline">

                                    @php
                                        $i = 1;
                                    @endphp
                                    @foreach($logs as $log)
                                        <div class="timeline-wrapper {{ $i % 2 == 0 ? 'timeline-inverted timeline-wrapper-warning' : 'timeline-wrapper-success' }} ">
                                            <div class="timeline-badge"></div>
                                            <div class="timeline-panel">
                                                <div class="timeline-heading">
                                                    <h6 class="timeline-title"><b>{{$log->title}}</b></h6>
                                                </div>
                                                <div class="timeline-body">
                                                    <p>
                                                        {{$log->body}}
                                                    </p>
                                                </div>
                                                <div class="timeline-footer d-flex align-items-center">
                                                    <span class="ml-auto font-weight-bold">{{ $log->created_at->diffForHumans() }}</span>
                                                </div>
                                            </div>
                                        </div>

                                        @php
                                            $i++;
                                        @endphp
                                    @endforeach
                                </div>
                            </div>

                            <div class="view-all-logs text-center">
                                <a href="{{route('admin.logs')}}" class="btn btn-default text-success">View All Logs</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}
        </div>

    </div>
@endsection