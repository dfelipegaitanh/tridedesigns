jQuery(document).ready(function () {
    $(".summernote").summernote({ height: 350,
        onImagesUpload: function(files, editor, $editable){
            sendFile(files[0],editor,$editable);
        },
         minHeight: null, maxHeight: null, focus: !1 }), $(".inline-editor").summernote({ airMode: !0 });
}),
    (window.edit = function () {
        $(".click2edit").summernote();
    }),
    (window.save = function () {
        $(".click2edit").summernote("destroy");
    });
