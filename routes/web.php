<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// Route::get('/contact', function () {
//     return view('admin.frontend.contact');
// });

Auth::routes();


//routes for admin
Route::get('/home', 'AdminController@home')->name('admin.home')->middleware('auth');

Route::group(['middleware' => ['auth'], 'prefix' => 'admin',], function () {
    Route::get('/admin', 'AdminController@home');
    Route::get('/contact', 'ContactusController@index');
    Route::post('contact_info/update/{id}', 'ContactusController@update');
    // about page content
    Route::get('/about', 'AboutController@index');
    Route::post('/update/about_content/{id}', 'AboutController@update_about_content');
    // team update
    Route::post('update/team/{id}', 'AboutController@update_team');
    Route::post('add/funfact/', 'AboutController@addFact');
    Route::get('funfact/delete/{id}', 'AboutController@deleteFact');

    // home page

    Route::get('/home', 'HomeController@homeSetting');
    Route::post('/home/update/{id}', 'HomeController@updatehomeSetting');

    //=============product option ============//
    Route::get('/products' , 'ProductController@index');
    Route::get('products/create' , 'ProductController@create');
    Route::post('products/store' , 'ProductController@store');
    Route::get('product/edit/{id}' , 'ProductController@edit');
    Route::post('products/update/{id}' , 'ProductController@update');
    Route::get('product/delete/{id}' , 'ProductController@destroy');

    Route::get('product/status/{id}/{status}' , 'ProductController@updatestatus');


    //=============collection option ============//
    Route::get('/collection' , 'CollectionController@index');
    Route::post('collection/store/', 'CollectionController@store');
    Route::get('collection/delete/{id}', 'CollectionController@destroy');
    Route::get('collection/edit/{id}' , 'CollectionController@edit');
    Route::post('collection/update/{id}' , 'CollectionController@update');

     //=============Order option ============//

     Route::get('orders', 'OrderManageController@index');
    Route::get('orders/view/{id}', 'OrderManageController@view');
    Route::get('orders/delete/{id}', 'OrderManageController@destroy');
     Route::get('orders/update/{id}/{status}' , 'OrderManageController@update');

    //=============Promotion option ============//
    Route::get('/promotion' , 'PromotionController@index');
    Route::post('promotion/store/', 'PromotionController@store');
    Route::get('promotion/delete/{id}', 'PromotionController@destroy');
    // Route::post('add/funfact/', 'AboutController@addFact');


    //=============Blog option ============//
    Route::get('/blogs' , 'BlogController@index');
    Route::get('blogs/create' , 'BlogController@create');
    Route::post('blogs/store' , 'BlogController@store');
    Route::get('blog/edit/{id}' , 'BlogController@edit');
    Route::post('blogs/update/{id}' , 'BlogController@update');
    Route::get('blog/delete/{id}' , 'BlogController@destroy');

    //=============Comments option ============//
    Route::get('/comments' , 'CommentController@index');
    Route::get('comments/create' , 'CommentController@create');
    Route::get('comments/update/{id}' , 'CommentController@update');
    Route::get('comment/delete/{id}' , 'CommentController@destroy');
     //=============Custom Order option ============//
    Route::get('/custom-orders' , 'CustomOrderController@index');
    Route::get('custom-orders/update/{id}' , 'CustomOrderController@update');
    Route::get('custom-orders/delete/{id}' , 'CustomOrderController@destroy');

});
Route::post('corder' , 'placeCustomOrder@store');
Route::post('applypromo' , 'ApplyPromoController@store')->name('applypromo'); //ajax promo
Route::get('/', 'HomeController@index')->name('landing');
Route::get('/contact', 'HomeController@contact');
Route::get('/about', 'HomeController@about');
Route::get('product/{slug}', 'HomeController@single_product');
Route::get('shop/{shop}', 'HomeController@single_collection');
Route::get('/blog', 'HomeController@blog');
Route::get('post/{slug}', 'HomeController@blogpost');
Route::post('post/store' , 'CommentController@store');
Route::get('/gallery', function(){
   return view('frontend.gallery');
});


  //============= Cart option ============//
Route::get('cart', 'CartController@viewCart')->name('cart.view');
Route::get('cart/stash/{id}', 'CartController@stashItem')->name("cart.stashItem");
Route::post('cart/updateQuantity', 'CartController@updateQuantity')->name('cart.updateSessionQuantity');
Route::post('cart/payNow', 'CartController@payNow')->name('cart.payNow');

//============= Order option ============//
Route::get('pay-with-paypal', 'PayPalController@payment')->name('paypal.payment');

Route::get('pay-with-card/{order}', 'StripeController@index')->name('stripe.payment');
Route::post('pay-with-card/{order}/checkout', 'StripeController@checkout')->name('stripe.payment.checkout');


Route::get('paypal/cancel', 'PayPalController@cancel')->name('payment.cancel');
Route::get('payment/success', 'PayPalController@success')->name('payment.success');
// Route::get('order/{order}/thanks', 'PayPalController@thanks')->name('order.thanks.page');
route::post('contact/me' , 'ContactController@store')->name('contact.me');


Route::get('/clear-cache', function () {
  Artisan::call('cache:clear');
  Artisan::call('view:clear');
  Artisan::call('config:cache');
  return 'cache cleared';
});

Route::get('/updateapp', function()
{
    \Artisan::call('dump-autoload');
    echo 'dump-autoload complete';
});


Route::get('storage_link', function(){
   Artisan::call('storage:link');
});

Route::view('/returns', 'frontend.returns')->name('returns');
Route::view('/privacy', 'frontend.privacy')->name('privacy');
Route::view('/terms', 'frontend.terms')->name('terms');
Route::view('/faqs', 'frontend.faqs')->name('faqs');
